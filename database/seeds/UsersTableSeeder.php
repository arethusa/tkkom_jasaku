<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = array(
        	['id' => 5, 'name' => 'SEPTIANO PRADITYA HARTONO', 'username' => 'admin.septiano', 'no_handphone' => '081324414747', 'status' => 'Admin', 'email' => 'septianopraditya19@gmail.com', 'password' => Hash::make('admin.septiano19'), 'created_at' => now(), 'token' => null],
        	);
        DB::table('users')->insert($users);
    }
}
