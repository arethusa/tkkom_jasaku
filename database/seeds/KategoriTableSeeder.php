<?php

use Illuminate\Database\Seeder;

class KategoriTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $dataKategoriJasa = array(
        	['id' => 1, 'nama_kategori_jasa' => 'Fotografi Dan Videografi'],
        	['id' => 2, 'nama_kategori_jasa' => 'Les Privat']

        	);
        DB::table('kategori_jasas')->insert($dataKategoriJasa);
    }
}
