<?php

use Illuminate\Database\Seeder;

class JasaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dataJasa = array(
            ['id' => 1, 'nama_jasa' => 'Dokumentasi Acara', 'deskripsi' => 'Dokumentasi dalam bentuk foto atau video dengan sudat pandang yang subjektif', 'gambar_jasa' => '6.jpg', 'user_id' => '2', 'kategori_jasa_id' => '1', 'harga_jasa' => '350000'],
            ['id' => 2, 'nama_jasa' => 'Foto Pernikahan', 'deskripsi' => 'Dokumentasi dalam bentuk foto dengan moment pernikahan', 'gambar_jasa' => '7.jpg', 'user_id' => '2', 'kategori_jasa_id' => '1', 'harga_jasa' => '2750000'],
            ['id' => 3, 'nama_jasa' => 'Foto Ulang Tahun', 'deskripsi' => 'Dokumentasi berupa foto dalam kegiatan ulangtahun', 'gambar_jasa' => '8.jpg', 'user_id' => '2', 'kategori_jasa_id' => '1', 'harga_jasa' => '225000'],
            ['id' => 4, 'nama_jasa' => 'Matematika', 'deskripsi' => 'Jasa berupa khursus mata pelajaran yang berfokus pada mata pelajaran Matematika', 'gambar_jasa' => '5.png', 'user_id' => '2', 'kategori_jasa_id' => '2', 'harga_jasa' => '90000'],
            ['id' => 5, 'nama_jasa' => 'Bahasa Inggris', 'deskripsi' => 'Jasa berupa khursus mata pelajaran yang berfokus pada mata pelajaran bahas Inggris', 'gambar_jasa' => '11.jpg', 'user_id' => '2', 'kategori_jasa_id' => '2', 'harga_jasa' => '120000'],
            ['id' => 6, 'nama_jasa' => 'Biologi', 'deskripsi' => 'Jasa berupa khursus mata pelajaran yang berfokus pada mata pelajaran Biologi (IPA)', 'gambar_jasa' => 'biologi.jpg', 'user_id' => '2', 'kategori_jasa_id' => '2', 'harga_jasa' => '80000']
            );
        DB::table('jasas')->insert($dataJasa);

    }
}
