<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransaksiJasasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trasaksi_jasas', function (Blueprint $table) {
            $table->integer('transaksi_id')->unsigned();
            $table->foreign('transaksi_id')->references('id')->on('transaksis');
            $table->integer('jasa_id')->unsigned();
            $table->foreign('jasa_id')->references('id')->on('jasas'); 
            $table->timestamps();
            $table->primary(['transaksi_id', 'jasa_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trasaksi_jasas');
    }
}
