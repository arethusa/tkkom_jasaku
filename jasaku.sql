-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 14, 2018 at 05:14 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jasaku`
--

-- --------------------------------------------------------

--
-- Table structure for table `history_pembelians`
--

CREATE TABLE `history_pembelians` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `jasa_id` int(10) UNSIGNED NOT NULL,
  `qty` int(11) NOT NULL,
  `totalPrice` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `history_transaksis`
--

CREATE TABLE `history_transaksis` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jasas`
--

CREATE TABLE `jasas` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama_jasa` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `gambar_jasa` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `kategori_jasa_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `harga_jasa` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jasas`
--

INSERT INTO `jasas` (`id`, `nama_jasa`, `deskripsi`, `gambar_jasa`, `user_id`, `kategori_jasa_id`, `created_at`, `updated_at`, `harga_jasa`) VALUES
(1, 'Dokumentasi Acara', 'Dokumentasi dalam bentuk foto atau video dengan sudat pandang yang subjektif.', '6.jpg', 2, 1, NULL, NULL, '350000'),
(2, 'Foto Pernikahan', 'Dokumentasi dalam bentuk foto dengan moment pernikahan.', '7.jpg', 2, 1, NULL, NULL, '2750000'),
(3, 'Foto Ulang Tahun', 'Dokumentasi berupa foto dalam kegiatan ulangtahun.', '8.jpg', 2, 1, NULL, NULL, '225000'),
(4, 'Matematika', 'Jasa berupa khursus mata pelajaran yang berfokus pada mata pelajaran Matematika.', '5.png', 2, 2, NULL, NULL, '90000'),
(5, 'Bahasa Inggris', 'Jasa berupa khursus mata pelajaran yang berfokus pada mata pelajaran bahas Inggris.', '11.jpg', 2, 2, NULL, NULL, '120000'),
(6, 'Biologi', 'Jasa berupa khursus mata pelajaran yang berfokus pada mata pelajaran Biologi (IPA).', 'biologi.jpg', 2, 2, NULL, NULL, '80000'),
(16, 'Event Documentation', 'Jasa yang memberikan layanan untuk  dokumentasi sebuah acara yang diinginkan.', '_MG_7499.JPG', 1, 3, '2018-05-03 00:47:09', '2018-05-03 00:47:09', '120000000'),
(17, 'Service Speaker', 'Jasa khusus untuk reparasi yang menyangkut segala hal tentang speaker.', '_MG_7527.JPG', 1, 4, '2018-05-03 00:49:12', '2018-05-03 00:49:12', '450000'),
(18, 'Car Wash and Wax', 'Jasa yang memberikan layanan cuci mobil sekaligus menawarkan jasa memperhalus mobil', '_MG_7498.JPG', 2, 3, '2018-05-03 07:10:14', '2018-05-03 07:10:14', '175000'),
(19, 'Bersih Bersih Taman Rumah', 'Jasa yang memberikan layanan jasa untuk membersihkan taman rumah anda.', '_MG_2309.JPG', 2, 6, '2018-05-03 07:43:32', '2018-05-03 07:43:32', '120000'),
(22, 'Antar Jemput SD', 'Jasa sewa antar jemput sekolah untuk Sekolah Dasar.', '_MG_2318.JPG', 4, 5, '2018-05-03 16:32:07', '2018-05-03 16:32:07', '125000'),
(23, 'Antar Jemput SMP', 'Jasa sewa antar jemput sekolah untuk Sekolah Menengah Pertama.', '_MG_2330.JPG', 4, 5, '2018-05-03 16:35:05', '2018-05-03 16:35:05', '135000'),
(24, 'Service Kulkas', 'Lorem', 'Capture.PNG', 1, 4, '2018-05-13 13:31:13', '2018-05-13 13:31:13', '500000');

-- --------------------------------------------------------

--
-- Table structure for table `kategori_jasas`
--

CREATE TABLE `kategori_jasas` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama_kategori_jasa` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kategori_jasas`
--

INSERT INTO `kategori_jasas` (`id`, `nama_kategori_jasa`, `created_at`, `updated_at`) VALUES
(1, 'Fotografi Dan Videografi', NULL, NULL),
(2, 'Les Privat', NULL, NULL),
(3, 'Otomotif', NULL, NULL),
(4, 'Perbaikan Rumah / Service', NULL, NULL),
(5, 'Antar Jemput', NULL, NULL),
(6, 'Rumah Tangga', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `kontak_kamis`
--

CREATE TABLE `kontak_kamis` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pesan` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_03_19_041440_create_pembayarans_table', 1),
(4, '2018_03_19_042858_create_kategori_jasas_table', 1),
(5, '2018_03_21_025208_create_rekenings_table', 1),
(6, '2018_03_21_031400_create_history_transaksis_table', 1),
(7, '2018_03_21_031910_create_transaksis_table', 1),
(8, '2018_03_21_034007_create_jasas_table', 1),
(9, '2018_03_21_034136_create_recents_table', 1),
(10, '2018_03_21_034256_create_transaksi_jasa_table', 1),
(11, '2018_04_08_061857_alter_table_jasas', 2),
(12, '2018_05_06_231158_alter_users_table', 3),
(13, '2018_05_07_115338_create_history_pembelians_table', 4),
(14, '2018_05_12_182502_alter_users_table_cashier', 4),
(15, '2018_05_12_182930_create_subscriptions_table', 5),
(16, '2018_05_13_193937_create_pembayarans_table', 6),
(17, '2018_05_13_194047_create_transaksis_table', 7),
(18, '2018_05_13_194146_create_transaksi_jasas_table', 7),
(19, '2018_05_17_092836_create_kontak_kamis_table', 8),
(20, '2018_05_17_132003_create_testimonials_table', 8);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pembayarans`
--

CREATE TABLE `pembayarans` (
  `id` int(10) UNSIGNED NOT NULL,
  `nomor_pembayaran` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `jasa_yang_disewa` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_pembayaran` int(11) NOT NULL,
  `nama_pemesan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_pemilik_rekening` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nomor_rekening` int(50) NOT NULL,
  `bukti_pembayaran` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qrcode` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pembayarans`
--

INSERT INTO `pembayarans` (`id`, `nomor_pembayaran`, `jasa_yang_disewa`, `total_pembayaran`, `nama_pemesan`, `alamat`, `nama_pemilik_rekening`, `nomor_rekening`, `bukti_pembayaran`, `qrcode`, `created_at`, `updated_at`) VALUES
(1, '+7vjcdp', 'Foto Pernikahan', 2750000, 'Septiano Praditya Hartono', 'Tenggilis', 'Septiano', 12345, 'Screenshot (4).png', 'Septiano Praditya Hartono2018121421371494320zP2L.png', '2018-12-14 14:37:14', '2018-12-14 14:37:14'),
(2, '+7qihntc', 'Foto Pernikahan', 2750000, 'Septiano Praditya Hartono', 'Surabaya', 'thusa', 12345, 'Screenshot (7).png', 'Septiano Praditya Hartono2018121421443020284B53A.png', '2018-12-14 14:44:31', '2018-12-14 14:44:31'),
(3, '+7uohcx\0', 'Dokumentasi Acara', 3450000, 'Septiano Praditya Hartono', 'Surabaya', 'Septiano', 12345, 'Screenshot (7).png', 'Septiano Praditya Hartono2018121423025424358LVWJ.png', '2018-12-14 16:02:54', '2018-12-14 16:02:54');

-- --------------------------------------------------------

--
-- Table structure for table `recents`
--

CREATE TABLE `recents` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `jasa_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rekenings`
--

CREATE TABLE `rekenings` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama_bank` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_rekening` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_rekening` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `subscriptions`
--

CREATE TABLE `subscriptions` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stripe_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stripe_plan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `trial_ends_at` timestamp NULL DEFAULT NULL,
  `ends_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rating` double NOT NULL,
  `judul_testi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pesan_testi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `transaksis`
--

CREATE TABLE `transaksis` (
  `id` int(10) UNSIGNED NOT NULL,
  `subtotal` int(11) NOT NULL,
  `grandtotal` int(11) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `pembayaran_id` int(10) UNSIGNED NOT NULL,
  `history_transaksi_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `trasaksi_jasas`
--

CREATE TABLE `trasaksi_jasas` (
  `transaksi_id` int(10) UNSIGNED NOT NULL,
  `jasa_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_handphone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stripe_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_brand` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_last_four` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trial_ends_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `no_handphone`, `status`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `token`, `stripe_id`, `card_brand`, `card_last_four`, `trial_ends_at`) VALUES
(1, 'Septiano Praditya Hartono', 'septiano19', '081324414747', 'penjual-pembeli', 's@gmail.com', '$2y$10$.YEBNReY9erG3/XIEsvX7uK4iamYeJUG0qB64MiPfxcUsjIdgKrOe', 'AUfAkqzdvaW41wLgmid2WETPUVNE9ECoaYZpJdSwtmr3DJpQvp8yXdns1YC6', '2018-03-20 20:59:10', '2018-04-21 23:53:15', NULL, NULL, NULL, NULL, NULL),
(2, 'leoo', 'leo_ah', '123456789123', 'penjual-pembeli', 'leo123@yahoo.co.id', '$2y$10$g0T36bK39EdmrCrHJHBPduQsCC8EdAQqEovfRuRUoQinUQ6VRmzyO', 'myhk0mTFaBsEUB9JxoxOCPBrzgNlHF75N7fErTJhebjgpmF7AdBg9tWa5L7V', '2018-03-20 21:07:14', '2018-03-20 21:07:14', NULL, NULL, NULL, NULL, NULL),
(4, 'Indra Imanuel Gunawan', 'sumber_berkat', '081234567890', 'penjual-pembeli', 'sumber_berkat@yahoo.com', '$2y$10$xBz9Zr/qfCrim/sdEr7PcOXXSJ2UYkd0zIaWYvsP83tqr5exGUtzG', 'udwpGL1ypEgpANsYMY6DyOKczTRR33ze78h7nGzezgMksTKyuHaIqtQcdtkN', '2018-05-03 16:08:39', '2018-05-03 16:09:20', NULL, NULL, NULL, NULL, NULL),
(5, 'SEPTIANO PRADITYA HARTONO', 'admin.septiano', '081324414747', 'Admin', 'septianopraditya19@gmail.com', '$2y$10$0SmjGlLqOQBKue8Urmi4D.zvXAvGQwVKAui75UNolw//yS8D..YMK', 'AWiEOF9dAaNFWFngJWT5YBFxYf5bjEI0oPl5aSHHA4TVjdQHZ4tK7wqtjGoq', '2018-05-13 17:20:47', NULL, NULL, NULL, NULL, NULL, NULL),
(7, 'Septiano Praditya', 'septiano', '081234567890', 'penjual-pembeli', 'lm8_septiano@yahoo.co.id', '$2y$10$RZKc2FOM.oXpMw8J6MNXw.5LeOfMBcvNPdTEcwj.2VHBJCCNllKh2', '7P3ZvUvbCZRSayaLABt98Nz0jfQ5Ddav0YeMK0f29hKWVYNa580w95b06jkH', '2018-05-28 14:21:17', '2018-05-28 14:21:17', 'Pe2hoA4DV6571jPXwzEvFLK7V', NULL, NULL, NULL, NULL),
(8, 'Septiano Praditya', 'septiano191', '081234567890', 'penjual-pembeli', 's160415110@student.ubaya.ac.id', '$2y$10$7TqqRN22MoLZzfqRGOXlzePQBiepXSZdXg2OTuwtoQaPNcO6wsV/e', '4Zjympyz2oQvOU4zX01Qb6sv3epl8FNeAnjX6ROMQg9nL3slmE7DIsi6EGvy', '2018-05-28 14:33:38', '2018-05-28 14:33:38', 'l7ZoeG5138MLm3MaHTJ9ZpMym', NULL, NULL, NULL, NULL),
(9, 'Arethusa', 'thusthus', '082245280715', 'Admin', 'a@gmail.com', '$2y$10$4REiZNt58KF9FteBp5PNc.xbuJSKaNB23dscWfhZoLFB5GVx06jly', '6gALvW3z0z3y4oBjP0k8VaejmmQG1LxqogI24HyTMIODBzIaoQrlL35Mk46G', '2018-10-01 15:01:39', '2018-10-01 15:01:39', 'IeYFhLXGlew8JE3xVU9fXaNlL', NULL, NULL, NULL, NULL),
(11, 'Sergio', 'Aguero', '082245280715', 'penjual-pembeli', 'drycroak@gmail.com', '12345w==', NULL, '2018-11-26 03:03:02', '2018-11-26 03:03:02', 'tQxn8yaj5wzgwtKfJBTPHHaZM', NULL, NULL, NULL, NULL),
(12, 'Arethusa', 'arethsa', '082245280715', 'penjual-pembeli', 're@gmail.com', 'MTIzNDU2', NULL, '2018-11-26 03:52:47', '2018-11-26 03:52:47', '5pz6WKZjSEuhie4zOnTEKs6Sf', NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `history_pembelians`
--
ALTER TABLE `history_pembelians`
  ADD PRIMARY KEY (`id`),
  ADD KEY `history_pembelians_user_id_foreign` (`user_id`),
  ADD KEY `history_pembelians_jasa_id_foreign` (`jasa_id`);

--
-- Indexes for table `history_transaksis`
--
ALTER TABLE `history_transaksis`
  ADD PRIMARY KEY (`id`),
  ADD KEY `history_transaksis_user_id_foreign` (`user_id`);

--
-- Indexes for table `jasas`
--
ALTER TABLE `jasas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jasas_user_id_foreign` (`user_id`),
  ADD KEY `jasas_kategori_jasa_id_foreign` (`kategori_jasa_id`);

--
-- Indexes for table `kategori_jasas`
--
ALTER TABLE `kategori_jasas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kontak_kamis`
--
ALTER TABLE `kontak_kamis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pembayarans`
--
ALTER TABLE `pembayarans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `recents`
--
ALTER TABLE `recents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `recents_user_id_foreign` (`user_id`),
  ADD KEY `recents_jasa_id_foreign` (`jasa_id`);

--
-- Indexes for table `rekenings`
--
ALTER TABLE `rekenings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscriptions`
--
ALTER TABLE `subscriptions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaksis`
--
ALTER TABLE `transaksis`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transaksis_user_id_foreign` (`user_id`),
  ADD KEY `transaksis_pembayaran_id_foreign` (`pembayaran_id`),
  ADD KEY `transaksis_history_transaksi_id_foreign` (`history_transaksi_id`);

--
-- Indexes for table `trasaksi_jasas`
--
ALTER TABLE `trasaksi_jasas`
  ADD PRIMARY KEY (`transaksi_id`,`jasa_id`),
  ADD KEY `trasaksi_jasas_jasa_id_foreign` (`jasa_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `history_pembelians`
--
ALTER TABLE `history_pembelians`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `history_transaksis`
--
ALTER TABLE `history_transaksis`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jasas`
--
ALTER TABLE `jasas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `kategori_jasas`
--
ALTER TABLE `kategori_jasas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `kontak_kamis`
--
ALTER TABLE `kontak_kamis`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `pembayarans`
--
ALTER TABLE `pembayarans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `recents`
--
ALTER TABLE `recents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rekenings`
--
ALTER TABLE `rekenings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `subscriptions`
--
ALTER TABLE `subscriptions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `transaksis`
--
ALTER TABLE `transaksis`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `history_pembelians`
--
ALTER TABLE `history_pembelians`
  ADD CONSTRAINT `history_pembelians_jasa_id_foreign` FOREIGN KEY (`jasa_id`) REFERENCES `jasas` (`id`),
  ADD CONSTRAINT `history_pembelians_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `jasas` (`id`);

--
-- Constraints for table `history_transaksis`
--
ALTER TABLE `history_transaksis`
  ADD CONSTRAINT `history_transaksis_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `jasas`
--
ALTER TABLE `jasas`
  ADD CONSTRAINT `jasas_kategori_jasa_id_foreign` FOREIGN KEY (`kategori_jasa_id`) REFERENCES `kategori_jasas` (`id`),
  ADD CONSTRAINT `jasas_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `recents`
--
ALTER TABLE `recents`
  ADD CONSTRAINT `recents_jasa_id_foreign` FOREIGN KEY (`jasa_id`) REFERENCES `jasas` (`id`),
  ADD CONSTRAINT `recents_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `jasas` (`id`);

--
-- Constraints for table `transaksis`
--
ALTER TABLE `transaksis`
  ADD CONSTRAINT `transaksis_history_transaksi_id_foreign` FOREIGN KEY (`history_transaksi_id`) REFERENCES `history_transaksis` (`id`),
  ADD CONSTRAINT `transaksis_pembayaran_id_foreign` FOREIGN KEY (`pembayaran_id`) REFERENCES `pembayarans` (`id`),
  ADD CONSTRAINT `transaksis_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `trasaksi_jasas`
--
ALTER TABLE `trasaksi_jasas`
  ADD CONSTRAINT `trasaksi_jasas_jasa_id_foreign` FOREIGN KEY (`jasa_id`) REFERENCES `jasas` (`id`),
  ADD CONSTRAINT `trasaksi_jasas_transaksi_id_foreign` FOREIGN KEY (`transaksi_id`) REFERENCES `transaksis` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
