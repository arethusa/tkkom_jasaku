@extends('layouts.layout-admin')
@section('Title')
	Jasaku ID | Admin | Daftar Bukti Pembayaran
@endsection
@section('content-admin')
<div id="page-wrapper">
    <div class="row" style="margin-top: 30px;">
        <div class="col-lg-12">
            <h1 class="page-header">Daftar Bukti Pembayaran</h1>
        </div>
        <!-- /.col-lg-12 -->        
    </div>
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Daftar Bukti Pembayaran
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>Nomor</th>
                                <th>Nomor Pembayaran</th>
                                <th>Total Pembayaran</th>
                                <th>Nama Pemesan</th>
                                <th>Nama Pemilik Rekining</th>
                                <th>Nomor Rekening</th>
                                <th>Bukti Pembayaran</th>
                                <th>Waktu</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        
                        <tbody>
                            @foreach($pembayaran as $key => $pembayaran)
                            <tr class="odd gradeX">
                                <td>{{ $key+1 }}</td>
                                <td>{{ $pembayaran->nomor_pembayaran }}</td>
                                <td>{{ $pembayaran->jasa_yang_disewa }}</td>
                                <td>{{ $pembayaran->nama_pemesan }}</td>
                                <td>{{ $pembayaran->nama_pemilik_rekening }}</td>
                                <td>{{ $pembayaran->nomor_rekening }}</td>
                                <td><img src="{{ asset('images/Pembayaran-user/'.$pembayaran->bukti_pembayaran) }}" style="width: 500px; height: 150px;" class="img-responsive" /></td>
                                <td>Rp. {{ number_format($pembayaran->total_pembayaran, 2) }}</td>
                                <td>
                                    <!-- <form action="#" method="POST">
                                        {{ csrf_field() }} -->
                                        <input type="submit" value="Konfirmasi" class="btn btn-primary" name="submit"/>
                                    <!-- </form> -->
                                </td>
                            </tr>
                            @endforeach
                        </tbody>                        
                    </table>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div>
@endsection