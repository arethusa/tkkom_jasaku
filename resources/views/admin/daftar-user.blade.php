@extends('layouts.layout-admin')
@section('Title')
	Jasaku ID | Admin
@endsection
@section('content-admin')
<div id="page-wrapper">
    <div class="row" style="margin-top: 30px;">
        <div class="col-lg-12">
            <h1 class="page-header">Daftar User</h1>
        </div>
        <!-- /.col-lg-12 -->        
    </div>
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Daftar User
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>Nomor</th>
                                <th>Username</th>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>Status</th>
                                <th>No. Handphone</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        
                        <tbody>
                            @foreach($user as $key => $user)
                            <tr class="odd gradeX">
                                <td>{{ $key+1 }}</td>
                                <td>{{ $user->username }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->status }}</td>
                                <td>{{ $user->no_handphone }}</td>
                                <td>
                                    <form action="{{ route('daftar-user.destroy', $user->id) }}" method="POST">
                                        {{ method_field("DELETE") }}
                                        {{ csrf_field() }}
                                        <input type="submit" value="Hapus" class="btn btn-primary" name="submit"/>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>                        
                    </table>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div>
@endsection