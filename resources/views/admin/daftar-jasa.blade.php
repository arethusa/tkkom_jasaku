@extends('layouts.layout-admin')
@section('Title')
	Jasaku ID | Admin | Daftar Jasa
@endsection
@section('content-admin')
<div id="page-wrapper">
    <div class="row" style="margin-top: 30px;">
        <div class="col-lg-12">
            <h1 class="page-header">Daftar Jasa</h1>
        </div>
        <!-- /.col-lg-12 -->        
    </div>
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Daftar Jasa
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>Nomor</th>
                                <th>Nama</th>
                                <th>Deskripsi</th>
                                <th>Gambar</th>
                                <th>Harga</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        
                        <tbody>
                            @foreach($jasa as $key => $jasa)
                            <tr class="odd gradeX">
                                <td>{{ $key+1 }}</td>
                                <td>{{ $jasa->nama_jasa }}</td>
                                <td>{{ $jasa->deskripsi }}</td>
                                <td><img src="{{ asset('images/Jasa-user/'.$jasa->gambar_jasa) }}" style="width: 500px; height: 150px;" class="img-responsive" /></td>
                                <td class="center">Rp {{ number_format($jasa->harga_jasa, 2) }}</td>
                                <td>
                                    <form action="{{ route('daftar-jasa.destroy', $jasa->id) }}" method="POST">
                                        {{ method_field("DELETE") }}
                                        {{ csrf_field() }}
                                        <input type="submit" value="Hapus" class="btn btn-primary" name="submit"/>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>                        
                    </table>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div>
@ensection