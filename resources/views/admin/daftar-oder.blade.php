@extends('layouts.layout-admin')
@section('Title')
	Jasaku ID | Admin
@endsection
@section('content-admin')
<div id="page-wrapper">
    <div class="row" style="margin-top: 30px;">
        <div class="col-lg-12">
            <h1 class="page-header">Daftar Order</h1>
        </div>
        <!-- /.col-lg-12 -->        
    </div>
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Daftar Order
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>Nomor</th>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>Nama Jasa</th>
                                <th>Sub Total</th>
                                <th>Grand Total</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        
                        <tbody>
                            <tr class="odd gradeX">
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>
                                    <!-- <form action="#" method="POST">
                                        {{ method_field("DELETE") }}
                                        {{ csrf_field() }} -->
                                        <input type="submit" value="Batalkan" class="btn btn-primary" name="submit"/>
                                    <!-- </form> -->
                                </td>
                            </tr>
                        </tbody>                        
                    </table>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div>
@endsection