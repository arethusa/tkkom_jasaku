@extends('layouts.layout')
@section('Title')
	Jasaku ID | Riwayat
@endsection
@section('Content')

<!-- breadcrumbs -->
<div class="breadcrumbs">
	<div class="container">
		<ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
			<li><a href="{{ url('index') }}"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Beranda</a></li>
			<li class="active">Riwayat</li>
		</ol>
	</div>
</div>
<!-- checkout -->
<div class="checkout">
	<div class="container">
	<h3 class="animated wow slideInLeft" data-wow-delay=".5s">Daftar Riwayat</h3>
		<?php $no=0 ?>
	<!-- Cart @Thusa -->
		<div class="checkout-right animated wow slideInUp" data-wow-delay=".5s">
			<div style="overflow-x:auto;" class="table-responsive">
				<table class="table table-striped table-bordered">
					<thead>
						<tr class="rem1">
							<th>No.</th>
							<th>Nomor Pembayaran</th>
							<th>Jasa yang disewa</th>
							<th>Total Pembayran</th>	
							<th>Nama Pemesan</th>
							<th>Alamat</th>
						</tr>
					</thead>
					@foreach($pembayaran as $key => $pembayarans)
					<tr class="rem1">
						
						<td class="invert">{{ $key+1 }}</td>
						<td class="invert-image"><img src="{{ asset('/qr-code/'.$pembayarans->qrcode) }}" style='width: 150px; height: 150px;'/></a></td>
						<td class="invert">{{ $pembayarans['jasa_yang_disewa'] }}</td>
						<td class="invert">{{ $pembayarans['total_pembayaran'] }}</td>
						<td class="invert">{{ $pembayarans['nama_pemesan'] }}</td>
						<td class="invert">{{ $pembayarans['alamat'] }}</td>
					</tr>
					@endforeach
				</table>
			</div>
		</div>
	<!-- Cart @Thusa -->
	<br><br><br><br>
		<br>
	</div>
</div>
@endsection