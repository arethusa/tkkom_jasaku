@extends('layouts.layout-editprofil')
@section('Title1')
    Jasaku ID | Login
@endsection
@section('Content-Form')
<!-- breadcrumbs -->
    <div class="breadcrumbs">
        <div class="container">
            <ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
                <li><a href="{{ url('index') }}"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Beranda</a></li>
                <li class="active">Halaman Login</li>
            </ol>
        </div>
    </div>
<!-- //breadcrumbs -->
<!-- login -->
    <div class="login">
        <div class="container">
            <h3 class="animated wow zoomIn" data-wow-delay=".5s" style="font-size: 18px;">Halaman Login</h3>
            <p class="est animated wow zoomIn" data-wow-delay=".5s">Silahkan login disini</p>
            <div class="login-form-grids animated wow slideInUp" data-wow-delay=".5s">
                  <form method="POST" action="{{ route('login') }}">
                     {{ csrf_field() }}
                        <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" required autofocus placeholder="Username">

                        @if ($errors->has('username'))
                            <span class="help-block">
                                <strong>{{ $errors->first('username') }}</strong>
                            </span>
                        @endif

                        <input id="password" type="password" class="form-control" name="password" required placeholder="Kata Sandi">

                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                        <input type="submit" class="btn btn-primary" value="Masuk">
                    </form>
            </div>
            <h4 class="animated wow slideInUp" data-wow-delay=".5s">Belum Punya Akun?</h4>
            <p class="animated wow slideInUp" data-wow-delay=".5s"><a href="{{ url('register') }}">Daftar</a> atau kembali ke <a href="{{ url('/index') }}">Beranda</a></p>
        </div>
    </div>
<!-- //login -->
@endsection