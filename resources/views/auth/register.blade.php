@extends('layouts.layout-editprofil')
@section('Title1')
    Jasaku ID | Register
@endsection
@section('Content-Form')
<!-- breadcrumbs -->
    <div class="breadcrumbs">
        <div class="container">
            <ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
                <li><a href="{{ url('index') }}"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Beranda</a></li>
                <li class="active">Halaman Daftar</li>
            </ol>
        </div>
    </div>
<!-- //breadcrumbs -->
<!-- register -->
    <div class="register">
        <div class="container">
            <h3 class="animated wow zoomIn" data-wow-delay=".5s">Daftar Disini</h3>
            <p class="est animated wow zoomIn" data-wow-delay=".5s">Silahkan masukkan data - data berikut: </p>
            <div class="login-form-grids">
                <h5 class="animated wow slideInUp" data-wow-delay=".5s">Informasi Profil</h5>
                <form class="animated wow slideInUp" data-wow-delay=".5s" method="POST" action="{{ route('register') }}">
                 {{ csrf_field() }} 
                    <input type="text" placeholder="Nama Lengkap" required autofocus class="form-group{{ $errors->has('name') ? ' has-error' : '' }}" id="name" class="form-control" name="name">
                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif

                    <input type="text" placeholder="Username" required autofocus class="form-group{{ $errors->has('username') ? ' has-error' : '' }}" id="username" class="form-control" name="username">
                        @if ($errors->has('username'))
                            <span class="help-block">
                                <strong>{{ $errors->first('username') }}</strong>
                            </span>
                        @endif

                    <input type="email" placeholder="Alamat Email" class="form-group{{ $errors->has('email') ? ' has-error' : '' }}"  id="email" name="email"  value="{{ old('email') }}" required>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif

                    <input type="text" placeholder="Nomor Handphone" class="form-group{{ $errors->has('nomor handphone') ? ' has-error' : '' }}"  id="no_handphone" name="no_handphone"  value="{{ old('nomor handphone') }}" required>
                    @if ($errors->has('no_handphone'))
                        <span class="help-block">
                            <strong>{{ $errors->first('no_handphone') }}</strong>
                        </span>
                    @endif

                    <input type="password" placeholder="Kata Sandi" required class="form-group{{ $errors->has('password') ? ' has-error' : '' }}" id="password" name="password">
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif

                    <input type="password" placeholder="Konfirmasi Kata Sandi" required id="password-confirm" name="password_confirmation">

                    <input type="submit" class="btn btn-primary" value="Daftar">
                </form>
            </div>
        </div>
    </div>
<!-- //register -->
@endsection
