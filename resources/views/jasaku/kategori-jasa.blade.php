@extends('layouts.layout')
@section('Title')
	Jasaku ID | Fotografi dan Videografi | Dokumentasi Acara
@endsection
@section('Content')
<!-- breadcrumbs -->
	<div class="breadcrumbs">
		<div class="container">
			<ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
				<li><a href="{{ url('index') }}"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Beranda</a></li>
				@foreach($kategoriBerdasarkanId->take(1) as $detail)
				<li class="">{{ $detail->nama_kategori }}</li>
				@endforeach
			</ol>
		</div>
	</div>
<!-- //breadcrumbs -->
	
	<div class="new-collections">
		<div class="container">
			<div class="col-md-20 products-right">
				<!-- collections -->
				<div class="container">
					@foreach($kategoriBerdasarkanId as $detail)
						<ol class="breadcrumb breadcrumb1 animated wow slideInLeft">
							<div class="col-md-3 new-collections-grid">
								<div class="new-collections-grid1 new-collections-grid1-image-width animated wow slideInUp" data-wow-delay=".5s" >
									<div class="new-collections-grid1-image">
										<a href="single.html" class="product-image"><img src="{{ asset('images/Jasa-user/'.$detail->gambar_jasa) }}" alt=" " class="img-responsive"></a>
										<div class="new-collections-grid1-image-pos">
											<a href="#">Quick View</a>
										</div>
									</div>
									
									<h4><a href="#">{{$detail->nama_jasa}}</a></h4>
									<p>{{$detail->deskripsi}}</p>
									<h4 class="fontjasa">Rp {{ number_format($detail->harga_jasa, 2) }}</h4>
									<div class="simpleCart_shelfItem products-right-grid1-add-cart">
										<p><a class="item_add mid" href="{{ url('/cart/'.$detail->id_jasa) }}">Pilih </a></p>
									</div>
								</div>
								<div class="clearfix"> </div>
							</div>
						</ol>
					@endforeach
				</div>
				<!-- //collections -->
				<!-- menuslide -->
				<nav class="numbering animated wow slideInRight" data-wow-delay=".5s">
				  <ul class="pagination paging">
					<li>
					  <a href="#" aria-label="Previous">
						<span aria-hidden="true">&laquo;</span>
					  </a>
					</li>
					<li class="active"><a href="#">1<span class="sr-only">(current)</span></a></li>
					<li><a href="#">2</a></li>
					<li><a href="#">3</a></li>
					<li><a href="#">4</a></li>
					<li><a href="#">5</a></li>
					<li>
					  <a href="#" aria-label="Next">
						<span aria-hidden="true">&raquo;</span>
					  </a>
					</li>
				  </ul>
				</nav>
				<!-- //menuslide -->
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
@endsection