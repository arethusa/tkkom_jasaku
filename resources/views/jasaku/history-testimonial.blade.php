@extends('layouts.layout')
@section('Title')
	Jasaku ID | History Testimonial
@endsection
@section('Content')

<!-- breadcrumbs -->
<div class="breadcrumbs">
	<div class="container">
		<ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
			<li><a href="{{ url('index') }}"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Beranda</a></li>
			<li><a href="{{ route('isi-testimonial.index') }}"></span>Isi Testimonial</a></li>
			<li class="active">History Testimonial</li>
		</ol>
	</div>
</div>
<!-- //breadcrumbs -->	
<div class="checkout">
	<div class="container">
		<?php $no=0 ?>
		<div class="checkout-right animated wow slideInUp" data-wow-delay=".5s">
			<div style="overflow-x:auto;" class="table-responsive">
				<table class="table table-striped table-bordered">
					<thead>
						<tr class="rem1">
							<th>No.</th>
							<th>Nama</th>
							<th>E-mail</th>	
							<th>Alamat</th>
							<th>Rating</th>
							<th>Judul Testi</th>
							<th>Pesan Testi</th>
							<th>Tanggal Publish</th>
						</tr>
					</thead>
					@foreach($testi as $testimonial)
					<tr class="rem1">
						<?php $no+=1 ?>
						<td class="invert">{{ $no }}</td>
						<td class="invert">{{ $testimonial->nama }}</td>
						<td class="invert">{{ $testimonial->email }}</td>
						<td class="invert">{{ $testimonial->alamat }}</td>
						<td class="invert">{{ $testimonial->rating }}</td>
						<td class="invert">{{ $testimonial->judul_testi }}</td>
						<td class="invert">{{ $testimonial->pesan_testi }}</td>
						<td class="invert">{{ $testimonial->created_at }}</td>

					</tr>
					@endforeach
				</table>
			</div>
		</div>
	</div>
</div>
@endsection