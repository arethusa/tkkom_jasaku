@extends('layouts.layout')
@section('Title')
	Jasaku ID | Isi Testimonial
@endsection
@section('Content')
<!-- breadcrumbs -->
	<div class="breadcrumbs">
		<div class="container">
			<ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
				<li><a href="index.html"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Beranda</a></li>
				<li class="active">Testimonial</li>
			</ol>
		</div>
	</div>
<!-- //breadcrumbs -->
<!-- mail -->
	<form method="POST" action="{{ route('isi-testimonial.store') }}">
		{{ csrf_field() }}
		<div class="mail animated wow zoomIn" data-wow-delay=".5s">
		<div class="container">
			<h2 style="font-size: 48px;" class="mid">Isi Testimonial</h2>
			<hr>
			<p class="est">Isi testimonial setelah menyewa jasa bahwa <b>JasakuID</b> adalah market place yang menjanjikan. mohon kirimkan kritik dan saran dengan menggunakan format dibawah ini. Testimonial yang anda kirimkan akan membantu kami dalam pengembangan <b>JasakuID</b> dan juga membantu membuat jasa yang ada pada market place ini menjadi terpercaya. <br><br> <strong>Cek <a href="{{ url('/history-testimonial') }}" style="text-align: center;">disini</a> untuk melihat beberapa testimnonial.</strong></p>

			<div class="mail-grids">
				<div class="col-md-12 mail-grid-left animated wow slideInLeft" data-wow-delay=".5s">
					<div class="col-xs-12">
						<div class="form-group">
							<input type="text" id="nama" name="nama" required="" placeholder="Nama">
						</div>
					</div>
					<div class="col-xs-12">
						<div class="form-group">
							<input type="email" id="email" name="email" required="" placeholder="Email">		
						</div>
					</div>
					<div class="col-xs-12">
						<div class="form-group">
							<input type="text" id="alamat" name="alamat" required="" placeholder="Alamat Anda">
						</div>
					</div>
					<div class="col-xs-6">
						<div class="form-group">
							<select name="rating" id="rating" class="form-control" required placeholder="Rating">
                        		<option value="0.0" class="yes">0.0</option>
                        		<option value="1.0" class="yes">1.0</option>
                        		<option value="2.0" class="yes">2.0</option>
                        		<option value="3.0" class="yes">3.0</option>
                        		<option value="4.0" class="yes">4.0</option>
                        		<option value="5.0" class="yes">5.0</option>
                    		</select>		
						</div>
					</div>
					<div class="col-xs-12">
						<div class="form-group">
							<input type="text" id="judul_testi" name="judul_testi" required=""placeholder="Subject">		
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<textarea type="text" id="pesan_testi" name="pesan_testi" required="" placeholder="Pesan Anda"></textarea>	
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<input type="submit" value="Kirim Testimonial" >
						</div>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
		</div>
	</form>
<!-- //mail -->
@endsection

