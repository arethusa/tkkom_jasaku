@extends('layouts.layout-editprofil')
@section('Title1')
    Jasaku ID | Edit Profil
@endsection
@section('Content-Form')
<!-- register -->
    <div class="register">
        <div class="container">
            <h3 class="animated wow zoomIn" data-wow-delay=".5s">Edit Profil</h3>
            <div class="login-form-grids">
                <h5 class="animated wow slideInUp" data-wow-delay=".5s">Informasi Profil</h5>
                <form class="animated wow slideInUp" data-wow-delay=".5s" method="POST" action="{{ route('edit-profil.update', $profil->id) }}">
                 {{ csrf_field() }}
                 {{ method_field("PUT") }} 
                    <input type="text" placeholder="Nama Lengkap" required autofocus class="form-group{{ $errors->has('name') ? ' has-error' : '' }}" id="name" class="form-control" name="name" value="{{ $profil->name }}">
                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif

                    <input type="text" placeholder="Username" required autofocus class="form-group{{ $errors->has('username') ? ' has-error' : '' }}" id="username" class="form-control" name="username" value="{{ $profil->username }}">
                        @if ($errors->has('username'))
                            <span class="help-block">
                                <strong>{{ $errors->first('username') }}</strong>
                            </span>
                        @endif

                    <input type="email" placeholder="Alamat Email" class="form-group{{ $errors->has('email') ? ' has-error' : '' }}"  id="email" name="email" required value="{{ $profil->email }}">
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif

                    <input type="text" placeholder="Nomor Handphone" class="form-group{{ $errors->has('nomor handphone') ? ' has-error' : '' }}"  id="no_handphone" name="no_handphone" value="{{ $profil->no_handphone }}" required>
                    @if ($errors->has('no_handphone'))
                        <span class="help-block">
                            <strong>{{ $errors->first('no_handphone') }}</strong>
                        </span>
                    @endif

                    <input type="submit" class="btn btn-primary" value="Simpan">
                </form>
              <!--   <input type="submit" class="btn btn-primary" value="Ganti Password" onclick="window.location.href='{{ url('index') }}'"> -->
            </div>
        </div>
    </div>
<!-- //register -->
@endsection