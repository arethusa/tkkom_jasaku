@extends('layouts.layout')
@section('Title')
	Jasaku ID | Kontak Kami
@endsection
@section('Content')
<!-- breadcrumbs -->
	<div class="breadcrumbs">
		<div class="container">
			<ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
				<li><a href="index.html"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Beranda</a></li>
				<li class="active">Kontak Kami</li>
			</ol>
		</div>
	</div>
<!-- //breadcrumbs -->
<!-- mail -->
	<form method="POST" action="{{ route('kontak-kami.store') }}">
		{{ csrf_field() }}
		<div class="mail animated wow zoomIn" data-wow-delay=".5s">
		<div class="container">
			<h2 style="font-size: 48px;" class="mid">Kontak Kami</h2>
			<hr>
			<p class="est">Bila menemui masalah dalam penggunaan <b>JasakuID</b>, mohon kirimkan kritik dan saran dengan menggunakan format dibawah ini. Kritik dan saran yang anda kirimkan akan membantu kami dalam pengembangan <b>JasakuID</b> menjadi lebih baik. </p>
			<div class="mail-grids">
				<div class="col-md-8 mail-grid-left animated wow slideInLeft" data-wow-delay=".5s">
					<form>
						<input type="text" id="nama" name="nama" required="" placeholder="Nama">
						<input type="email" id="email" name="email" required="" placeholder="Email">
						<input type="text" id="subject" name="subject" required=""placeholder="Subject">
						<textarea type="text" id="pesan" name="pesan" required="" placeholder="Pesan Anda"></textarea>
						<input type="submit" value="Kirim Pesan" >
					</form>
				</div>
				<div class="col-md-4 mail-grid-right animated wow slideInRight" data-wow-delay=".5s">
					<div class="mail-grid-right1">
						<img src="images/logo_tab.png" alt=" " class="img-responsive" />
						<h4> Customer Service</h4>
						<ul class="phone-mail">
							<li><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i>Telp: 081990102911</li>
							<li><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i>Email: <a href="mailto:jasakuid@gmail.com">jasakuid@gmail.com</a></li>
						</ul>
						<ul class="social-icons">
							<li><a href="#" class="facebook"></a></li>
							<li><a href="#" class="twitter"></a></li>
							<li><a href="#" class="g"></a></li>
							<li><a href="http://www.instagram.com/jasakuid" class="instagram"></a></li>
						</ul>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
		</div>
	</form>
<!-- //mail -->
@endsection

