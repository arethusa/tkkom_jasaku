@extends('layouts.layout')
@section('Title')
	Jasaku ID | Keranjang Anda
@endsection
@section('Content')

<!-- breadcrumbs -->
<div class="breadcrumbs">
	<div class="container">
		<ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
			<li><a href="{{ url('index') }}"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Beranda</a></li>
			<li class="active">Keranjang Anda</li>
		</ol>
	</div>
</div>
<!-- //breadcrumbs -->	
@if(Session::has('cart'))
<!-- checkout -->
<div class="checkout">
	<div class="container">
	<h3 class="animated wow slideInLeft" data-wow-delay=".5s">Daftar Jasa Yang Ada di Keranjang Anda</h3>
		<?php $no=0 ?>
	<!-- Cart @Thusa -->
		<div class="checkout-right animated wow slideInUp" data-wow-delay=".5s">
			<div style="overflow-x:auto;" class="table-responsive">
				<table class="table table-striped table-bordered">
					<thead>
						<tr class="rem1">
							<th>No.</th>
							<th>Foto</th>
							<th>Qty</th>	
							<th>Nama Jasa</th>
							<th>Harga Jasa</th>
							<th>Hapus</th>
						</tr>
					</thead>
					@foreach($jasas as $jasa)
					<tr class="rem1">
						<?php $no+=1 ?>
						<td class="invert">{{ $no }}</td>
						<td class="invert-image"><img src="{{ asset('images/Jasa-user/'.$jasa['item']['gambar_jasa']) }}" style='width: 300px; height: 150px;'/></a></td>
						<td class="invert">{{ $jasa['qty'] }}</td>
						<td class="invert">{{ $jasa['item']['nama_jasa'] }}</td>
						<td class="invert">Rp. {{ number_format($jasa['harga_jasa'], 2) }}</td>
						<td class="invert">
							<div class="rem">
								<div class="invert"><p><a class="item_add mid" href="{{ url('/remove/'.$jasa['item']['id']) }}">Hapus </a></p></div>
							</div>
						</td>
					</tr>
					@endforeach
					<tr class="rem1">
						<td colspan="4" class="alert alert-success" style="text-align: right; color: red">Total:</td>
						<td class="invert alert alert-success" style="color: red">Rp. {{ number_format($totalPrice, 2) }}</td>
						<td class="invert alert alert-success"></td>
					</tr>
				</table>
			</div>
		</div>
		<a href="{{ url('/checkout') }}" class="btn btn-primary animated wow slideInLeft" role="button" style="margin-left: 92%">Checkout</a>
	<!-- Cart @Thusa -->
	<br><br><br><br>
	<!-- Untuk UP-Selling @Septiano -->
	<h3 class="animated wow slideInLeft" data-wow-delay=".5s">Daftar Jasa Yang Sesuai Untuk Anda</h3>
		<?php $no1=0 ?>
		<div class="checkout-right animated wow slideInUp" data-wow-delay=".5s">
			<div style="overflow-x:auto;" class="table-responsive">
				<table class="table table-striped table-bordered">
					<thead>
						<tr class="rem1">
							<th>No.</th>
							<th>Foto</th>	
							<th>Nama Jasa</th>
							<th>Harga Jasa</th>
							<th>Aksi</th>
						</tr>
					</thead>
					@foreach($jasaYangSesuai as $jasa)
					<tr class="rem1">
						@if($jasa->kategori_id == $jasa->kategori_jasa_id)
							@if($totalPrice > $jasa->harga_jasa || $jasa->nama_jasa != $jasa->nama_jasa)
								<?php $no1+=1 ?>
								<td class="invert">{{ $no1 }}</td>
								<td class="invert-image"><a href="{{ url('/foto-pernikahan/'.$jasa->id_jasa)}}"><img src="{{ asset('images/Jasa-user/'.$jasa->gambar_jasa) }}" style='width: 300px; height: 150px;'/></a></td>
								<td class="invert">{{ $jasa->nama_jasa }}</td>
								<td class="invert">Rp {{ number_format($jasa->harga_jasa, 2) }}</td>
								<td class="invert"><a class="item_add" href="{{ url('/cart/'.$jasa->id_jasa) }}">Pilih </a></td>
							@endif
						@endif
					</tr>
					@endforeach
				</table>
			</div>
		</div>
	<!-- Untuk UP-Selling @Septiano -->
		<br>
	</div>
</div>
@else
<div class="checkout">
	<div class="container">
		<h3 class="animated wow slideInLeft" data-wow-delay=".5s">Daftar Jasa Yang Ada di Keranjang Anda</h3>
		<?php $no=0 ?>
		<div class="checkout-right animated wow slideInUp" data-wow-delay=".5s">
			<div style="overflow-x:auto;" class="table-responsive">
				<table class="table table-striped table-bordered">
					<thead>
						<tr class="rem1">
							<th>No.</th>
							<th>Foto</th>
							<th>Qty</th>	
							<th>Nama Jasa</th>
							<th>Harga Jasa</th>
							<th>Hapus</th>
						</tr>
					</thead>
					<tr>
						<td colspan="6" class="alert alert-success" style="text-align: center; color: red">Keranjang kosong.</td>
					</tr>
				</table>
			</div>
		</div>
		<br>
	</div>
</div>
@endif
@endsection