@extends('layouts.layout-editprofil')
@section('Title1')
    Jasaku ID | Tambah Jasa
@endsection
@section('Content-Form')

<!-- breadcrumbs -->
<div class="breadcrumbs">
    <div class="container">
        <ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
            <li><a href="{{ url('index') }}"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Beranda</a></li>
            <li class="active">Tambah Jasa</li>
        </ol>
    </div>
</div>
<!-- //breadcrumbs -->
<!-- register -->
    <div class="register">
        <div class="container">
            <h3 class="animated wow zoomIn" data-wow-delay=".5s">Tambahkan Jasa</h3>
            <div class="login-form-grids">
                <h5 class="animated wow slideInUp" data-wow-delay=".5s">Informasi Jasa yang Dibutuhkan</h5>
                <form enctype="multipart/form-data" class="animated wow slideInUp" data-wow-delay=".5s" method="POST" action="{{ route('tambah-jasa.store') }}">
                 {{ csrf_field() }}
                     <div class="form-group has-feedback {{ $errors->has('user_kategori') ? ' has-error' : '' }}">
                        <select name="user_kategori" id="user_kategori" class="form-control required_mahasiswa" required placeholder="Kategori Jasa">
                            @foreach($kategori as $kategoris) 
                            <option value="{{$kategoris->id}}" class="yes">{{$kategoris->nama}}</option>
                            @endforeach
                        </select>
                    </div>
                    <input type="text" placeholder="Nama Jasa" required autofocus id="nama_jasa" class="form-control" name="nama_jasa" value="">
                            <span class="help-block">
                                <strong></strong>
                            </span>
                    <textarea placeholder="Deskripsi" rows="5" id="deskripsi" class="form-control" name="deskripsi"></textarea>
                            <span class="help-block">
                                <strong></strong>
                            </span>
                    <hr class="line-tambah-jasa">
                    <fieldset class="container">
                        <div class="animated wow zoomIn">
                        <h5 class="animated wow slideInUp" data-wow-delay=".5s">Masukkan Gambar:</h5>
                        <input type="file" class="form-control-file" id="gambar_jasa" name="gambar_jasa">
                        <span class="help-block">
                            <strong></strong>
                        </span>
                        </div>
                    </fieldset>
                    <hr class="line-tambah-jasa">
                    <input type="text" placeholder="Harga Jasa" required autofocus class="" id="harga_jasa" class="form-control" name="harga_jasa" value="" data-affixes-stay="true" data-prefix="Rp. " data-thousands="," data-decimal=".">
                    <input type="submit" class="btn btn-primary" value="Simpan">
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $('input[id="none"]').on('change', function () {
            if (this.checked) {
              $('.addcategory').hide();     
            }
        });

            $('input[class="yes"]').on('change', function () {
            if (this.checked) {
              $('.addcategory').show();     
            }
        });
    </script>
    <script type="text/javascript" src="{{ asset('js/jquery.maskMoney.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#harga_jasa').maskMoney();
        });
    </script>
<!-- //register -->
@endsection