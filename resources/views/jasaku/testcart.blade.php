@extends('layouts.layout-editprofil')
@section('Title1')
    Jasaku ID | Tambah Jasa
@endsection
@section('Content-Form')

@if(Session::has('cart'))
	<div class="row">
		<div class="col-sm-6 col-md-6 col-md-offset-3 col-sm-offset-3">
			<ul class="list-group">
				@foreach($jasas as $jasa)
					<li>
						<span class="badge">{{ $jasa['qty'] }}</span>
						<strong>{{ $jasa['item']['nama_jasa'] }}</strong>
						<span class="label label-success">{{ $jasa['harga_jasa'] }}</span>
						<div class="btn-group">
							<button type="button" class="btn btn-primary btn-xs dropdown-toogle" data-toogle="dropdown">Action <span class="caret"></span></button>
							<ul class="dropdown-menu">
								<li><a href="">Reduce by 1</a></li>
								<li><a href="">Reduce All</a></li>
							</ul>
						</div>
					</li>
				@endforeach
			</ul>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6 col-md-6 col-md-offset-3 col-sm-offset-3">
			<strong>Total: {{ $totalPrice }}</strong>
		</div>
	</div>
	<hr>
	<div class="row">
		<div class="col-sm-6 col-md-6 col-md-offset-3 col-sm-offset-3">
			<button type="button" class="btn btn-success">Checkout</button>
		</div>
	</div>
@else
	<div class="row">
		<div class="col-sm-6 col-md-6 col-md-offset-3 col-sm-offset-3">
			<h2>No Cart</h2>
		</div>
	</div>
@endif
@endsection