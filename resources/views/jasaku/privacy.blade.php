@extends('layouts.layout')
@section('Title')
	Jasaku ID | Privacy Policy
@endsection
@section('Content')
<!-- breadcrumbs -->
	<div class="breadcrumbs">
		<div class="container">
			<ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
				<li><a href="index.html"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Beranda</a></li>
				<li class="active">Privacy Policy</li>
			</ol>
		</div>
	</div>
<!-- //breadcrumbs -->
		<div class="mail animated wow zoomIn" data-wow-delay=".5s">
		<div class="container">
			<h2 style="font-size: 28px;" class="mid">Privacy Policy</h2>
			<hr>		

			<strong>Privacy Policy</strong>
			<br><br>
			<p>This privacy policy sets out how Indogamecard uses and protects any information that you give Indogamecard when you use this website. Indogamecard is committed to ensuring that your privacy is protected. Should we ask you to provide certain information by which you can be identified when using this website, then you can be assured that it will only be used in accordance with this privacy statement. Indogamecard may change this policy from time to time by updating this page. You should check this page from time to time to ensure that you are happy with any changes. This policy is effective from 1 July 2015</p>

			<br><br>

			<strong>What we collect</strong>
			<br><br>

			We may collect the following information:
			<br>
			• name 
			<br>
			• contact information including email address
			<br>
			• demographic information such as postcode, preferences and interests
			<br>
			• other information relevant to customer surveys and/or offers
			<br>	
			</p>
			<br><br>

			<strong>What we do with the information we gather</strong>

			<p>We require this information to understand your needs and provide you with a better service, and in particular for the following reasons:
			• Internal record keeping.
			• We may use the information to improve our products and services.
			• We may periodically send promotional emails about new products, special offers or other information which we think you may find interesting using the email address which you have provided.
			• From time to time, we may also use your information to contact you for market research purposes. We may contact you by email, phone, fax or mail. We may use the information to customize the website according to your interests.
			</p>

			<br><br>

			<strong>Security</strong>
			<br><br>

			<p>We are committed to ensuring that your information is secure. In order to prevent unauthorized access or disclosure, we have put in place suitable physical, electronic and managerial procedures to safeguard and secure the information we collect online.
			</p>

			<br><br>

			<strong>Links to other websites</strong>

			<br><br>

			<p>Our website may contain links to other websites of interest. However, once you have used these links to leave our site, you should note that we do not have any control over that other website. Therefore, we cannot be responsible for the protection and privacy of any information which you provide whilst visiting such sites and such sites are not governed by this privacy statement. You should exercise caution and look at the privacy statement applicable to the website in question.
			</p>


			<strong>Controlling your personal information</strong>

			<br><br>

			<p>You may choose to restrict the collection or use of your personal information if you have previously agreed to us using your personal information for direct marketing purposes, you may change your mind at any time by writing to or emailing us at <strong>jasakuid@gmail.com</strong>. We will not sell, distribute or lease your personal information to third parties unless we have your permission or are required by law to do so. We may use your personal information to send you promotional information about third parties which we think you may find interesting if you tell us that you wish this to happen. If you believe that any information we are holding on you is incorrect or incomplete, please write to or email us as soon as possible at the above address. We will promptly correct any information found to be incorrect.</p>
		</div>
	</div>
@endsection

