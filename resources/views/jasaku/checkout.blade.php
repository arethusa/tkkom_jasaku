@extends('layouts.layout')
@section('Title')
	Jasaku ID | Checkout
@endsection
@section('Content')

<!-- breadcrumbs -->
<div class="breadcrumbs">
	<div class="container">
		<ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
			<li><a href="{{ url('index') }}"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Beranda</a></li>
			<li><a href="{{ url('/keranjang-anda') }}">Keranjang Anda</a></li>
			<li class="active">Checkout</li>
		</ol>
	</div>
</div>
<!-- //breadcrumbs -->	
@if(Session::has('cart'))
<div class="login-form-grids">
	<div class="row">
		<h3 class="animated wow slideInLeft" data-wow-delay=".5s">Daftar Jasa Yang Dipilih</h3>
		<br><br>
			@foreach($jasas as $jasa)
				<div class="col-md-4">
				<img src="{{ asset('images/Jasa-user/'.$jasa['item']['gambar_jasa']) }}" style="width: 80%; height: 100%;">
				</div>
				<strong>{{ $jasa['item']['nama_jasa'] }}</strong>
				<br>
				<span class="badge">{{ $jasa['qty'] }}</span> x
				<span class="label label-success">Rp. {{ number_format($jasa['harga_jasa'], 2) }}</span>
				<hr>
			@endforeach
	</div>
	
	<div class="row">
		<hr>
		<strong>Total: Rp. {{ number_format($totalPrice, 2) }}</strong>			
		<hr>
	</div>
	
	<div class="row">
		<a href="{{ url('/keranjang-anda') }}" class="btn btn-primary animated wow slideInLeft" role="button">Kembali ke Keranjang Anda</a>
		<br><br>
		<a href="{{ route('pembayaran-jasa.index') }}" class="btn btn-success animated wow slideInLeft" role="button">Konfirmasi Pembayaran</a>
	</div>
</div>
@endif
<br><br>
@endsection