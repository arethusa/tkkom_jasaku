@extends('layouts.layout')
@section('Title')
	Jasaku ID | Konfirmasi Pembayaran
@endsection
@section('Content')

<!-- breadcrumbs -->
<div class="breadcrumbs">
	<div class="container">
		<ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
			<li><a href="{{ url('index') }}"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Beranda</a></li>
			<li><a href="{{ url('/keranjang-anda') }}">Keranjang Anda</a></li>
			<li><a href="{{ url('/checkout') }}">Checkout</a></li>
			<li class="active">Konfirmasi Pembayaran</li>
		</ol>
	</div>
</div>
<!-- //breadcrumbs -->	
@if(Session::has('cart'))
<div class="row">
	<div class="col-sm-6 col-md-6 col-md-offset-3 col-sm-offset-3">
		<br><br>
		<div class="col-xs-12">
			<div class="form-group">
				<h3 class="animated wow slideInLeft mid" data-wow-delay=".5s">Konfirmasi Pembayaran</h3>
			</div>
		</div>
		<form enctype="multipart/form-data" method="POST" action="{{ route('pembayaran-jasa.store') }}" id="pembayaran">
			{{ csrf_field() }}
			<div class="col-xs-12">
				<div class="form-group">
					<strong style="color: red" name="total_pembayaran">Nomor Pembayaran: {{ $dateNow.$randomNum.$randomString }}</strong>
				</div>
			</div>
			<div class="col-xs-12">
				<div class="form-group">
					<strong style="color: red" name="total_pembayaran">Total Pembayaran: Rp. {{ number_format($totalPrice, 2) }}</strong>
				</div>
			</div>
			<br>
			<!-- Pembayaran@tHusA-->
			<div class="col-xs-12">
				<div class="form-group">
					<p>Nama Pemesan: </p>
					<input type="text" placeholder="Lionel Messi" required autofocus id="nama_pemesan" class="form-control" name="nama_pemesan">
				</div>
			</div>
			<div class="col-xs-12">
				<div class="form-group">
					<p>Alamat: </p>
					<input type="textarea" placeholder="659 s soto street los angeles ca 110030789580" required autofocus id="alamat" class="form-control" name="alamat">
				</div>
			</div>
			<div class="col-xs-6">
				<div class="form-group">
					<p>Nama Pemilik Rekening: </p>
					<input type="text" placeholder="A/N Lionel Messi" required autofocus id="nama_pemilik_rekening" class="form-control" name="nama_pemilik_rekening">		
				</div>
			</div>
			<div class="col-xs-6">
				<div class="form-group">
					<p>No. Rekening: </p>
					<input type="text" placeholder="Masukkan digit angka rekening anda" required autofocus id="nomor_rekening" class="form-control" name="nomor_rekening">
				</div>
			</div>
			<div class="col-xs-12">
				<div class="form-group">
	                <div class="animated wow zoomIn">
	                <p class="animated wow slideInUp" data-wow-delay=".5s">Masukkan Bukti Pembayaran:</p><br>
	                <input type="file" class="form-control-file" id="bukti_pembayaran" name="bukti_pembayaran">
	                <span class="help-block">
	                    <strong></strong>
	                </span>
	                </div>
	                <hr>
	                <div class="form-group">
						<strong style="color: red">HARAP DI PERHATIKAN.</strong>
						<p>Transfer ke Nomor Rekening: 13451148819293</p>
						<p>Jenis Bank: BCA</p>
						<p>A/N: John Wick</p>
					</div>
	                <hr>
				</div>
			</div>
			<div class="col-xs-12">
				<div class="form-group">
					<input type="submit" name="" class="btn btn-success" value="Confirm">
				</div>
			</div>
		</form>
	</div>
</div>
@endif
<br><br>
@endsection