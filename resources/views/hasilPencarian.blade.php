@extends('layouts.layout')
@section('Title')
	Jasaku ID
@endsection
@section('Content')

<!-- collections -->
<div class="new-collections">
	<div class="container">
		<h3 class="animated wow zoomIn" data-wow-delay=".5s">Hasil Pencarian</h3><br><br>
		@if($echo == "Pencarian tidak ditemukan")
			<h2 class="alert alert-danger" style="text-align: center;">{{$echo}}</h2>
			{{ $jasasayaSearch->appends(['s' => $s])->links() }}
		@else
		{{ $jasasayaSearch->appends(['s' => $s])->links() }}
		@foreach($jasasayaSearch as $jasa)
		<div class="breadcrumb breadcrumb1 animated wow slideInLeft">
			<div class="col-md-3 new-collections-grid marbot">
				<div class="new-collections-grid1 new-collections-grid1-image-width animated wow slideInUp inline" data-wow-delay=".5s">	
					<div class="new-collections-grid1-image">
						<a href="#" class="product-image"><img src="{{ asset('images/Jasa-user/'.$jasa->gambar_jasa) }}" alt=" " class="img-responsive"/></a>
						<div class="new-collections-grid1-image-pos">
							<a href="#">Quick View</a>
						</div>
					</div>
					<p style="font-size: 15px;"><a href="#">{{$jasa->nama_jasa}}</a></p>
					<p>{{$jasa->deskripsi}}</p>
					<h4 class="fontjasa">Rp {{ number_format($jasa->harga_jasa, 2) }}</h4>
					<div class="simpleCart_shelfItem products-right-grid1-add-cart">
						<p>
							<a class="item_add mid" href="{{ url('/cart/'.$jasa->id_jasa) }}">Pilih </a>
						</p>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
		@endforeach
		@endif
	</div>
</div>
<!-- //collections -->
@endsection