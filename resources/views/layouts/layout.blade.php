<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
<head>
<title>@yield('Title')</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Best Store Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet" type="text/css" media="all" />
<link href="{{ asset('css/style.css') }}"  rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" type="text/css" href="{{ asset('css/temp.css') }}">
<link rel="icon" href="{{ asset('images/logo_tab.png') }}">
<!-- js -->
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/jquery-ui.min.js') }}"></script>
<script scr="{{ asset('js/jquery-1.12.4.js') }}"></script>
<script src="{{ asset('js/autocomplete.js') }}"></script>
<script src="https://js.stripe.com/v3/"></script>
<!-- //js -->
<!-- cart -->
<script src="{{ asset('js/simpleCart.min.js') }}"></script>
<!-- cart -->
<!-- for bootstrap working -->
<script type="text/javascript" src="{{ asset('js/bootstrap-3.1.1.min.js') }}"></script>
<!-- timer -->
<link rel="stylesheet" href="{{ asset('css/jquery.countdown.css') }}" />
<link rel="stylesheet" href="{{ asset('css/test.css') }}" />
<!-- //timer -->
<!-- animation-effect -->
<link href="{{ asset('css/animate.min.css') }}" rel="stylesheet"> 
<link rel="stylesheet" href="{{ asset('css/jquery-ui.css') }}">
<script src="{{ asset('js/wow.min.js') }}"></script>
<script>
 new WOW().init();
</script>
<!-- //animation-effect -->
</head>
	
<body>
<!-- header -->
	<div class="header">
		<div class="container">
			<div class="header-grid">
				<div class="header-grid-left animated wow slideInLeft" data-wow-delay=".5s">
					<ul>
						@if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    	@endif
						@guest
						<li><i class="glyphicon glyphicon-log-in" aria-hidden="true"></i><a href="{{ route('login') }}">Masuk</a></li>
						<li><i class="glyphicon glyphicon-book" aria-hidden="true"></i><a href="{{ route('register') }}">Daftar</a></li>
						@else
						<li>
							<i class="glyphicon glyphicon-log-in" aria-hidden="true"></i><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
						</li>
						<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
	                                    {{ csrf_field() }}
                        </form>
						<p class="alert alert-success"><a href="{{ url('edit-profil/'.Auth::user()->id.'/edit') }}">{{ Auth::user()->name }}</a></p>
						@if(auth()->user()->verified() == true)
						<p class='alert alert-success'>Akun anda : Terverifikasi</p>
						@else
						<p class='alert alert-danger'>Akun anda : Belum terverifikasi</p>
						@endif
	                    @endguest
					</ul>
				</div>
				<div class="header-grid-right animated wow slideInRight" data-wow-delay=".5s">
					@if(Auth::check())
					<ul>
						<div class="simpleCart_shelfItem products-right-grid1-add-cart">
							<p><span class="#"></span><a class="#" href="{{ route('tambah-jasa.index') }}" style="color: black;">Tambah Jasa</a></p>
						</div>
					</ul>
					@endif
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="logo-nav">
				<div class="logo-nav-left animated wow zoomIn" data-wow-delay=".5s">
					<h1><a href="{{ url('/index') }}"><img src="{{ asset('images/logo.png') }}" alt=" " class="img-responsive" /></a></h1>
				</div>
				<div class="logo-nav-left1">
					<nav class="navbar navbar-default">
						<div class="navbar-header nav_2">
							<button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>					
						</div>
						<div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
							<ul class="nav navbar-nav">
								<li class="active"><a href="{{ url('/index') }}" class="act">Beranda</a></li>	
								<!-- Mega Menu -->
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown">Kategori Jasa <b class="caret"></b></a>
									<ul class="dropdown-menu multi-column columns-3">
										<div class="row">
											@foreach($kategori as $kategoris)
											<div class="col-sm-6">
												<ul class="multi-column-dropdown">
													<!-- Nama Kategori - SEPTIANO -->
													<h6><a href="{{ url('kategori-jasa/'.$kategoris->nama_kategori_jasa) }}">{{ $kategoris->nama_kategori_jasa}}</a></h6>

													<!-- Nama Jasa - SEPTIANO -->
													@foreach($jasasaya as $jasa)
													@if($jasa->kategori_id == $kategoris->id)
													<li><a href="{{ url('detail-jasa/'.$jasa->nama_jasa) }}">{{$jasa->nama_jasa}}</a></li>
													@endif
													@endforeach
												</ul>
											</div>
											@endforeach
											<div class="clearfix"></div>
										</div>
									</ul>
								</li>
								<li><a href="{{url('/kontakkami')}}">Kontak Kami</a></li>
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown">Keranjang Anda<b class="caret"></b>
										@if(Session::has('cart'))
										<span class="badge">{{ Session::has('cart') ? Session::get('cart')->totalQty : '' }} item</span>
										
										@else
										<span class="badge">{{ Session::has('cart') ? Session::get('cart')->totalQty : '' }} 0 item</span>
										@endif
									</a>
									<ul class="dropdown-menu multi-column columns-2	clearfix">
										<div class="row">
											<div class="col-sm-6">
												<ul class="multi-column-dropdown" style="size: width: 40px; height: 40px;">
													<li><a href="{{ url('/keranjang-anda') }}">Pergi ke Keranjang</a></li>
													<li><a href="{{ url('/checkout') }}">Checkout</a></li>
												</ul>
											</div>
											<div class="clearfix"></div>
										</div>
									</ul>
								</li>
								<li><a href="{{url('/history')}}">Riwayat</a></li>
							</ul>
						</div>
					</nav>
				</div>
				<div class="container">
					<div class="row">
						<form action="{{ url('/hasilPencarian') }}" method="GET">
							<div id="custom-search-input">
	                            <div class="input-group col-md-12">
	                                <input type="text" class="form-control" name="searchrow" id="searchrow" placeholder="Cari Jasa" value="{{ isset($s) ? $s : '' }}"/>
	                                <span class="input-group-btn">
	                                    <button class="btn btn-danger" type="submit">
	                                        <span class=" glyphicon glyphicon-search"></span>
	                                    </button>
	                                </span>
	                            </div>
	                        </div>
						</form>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
<!-- //header -->
@yield('Content')
<!-- footer -->
	<div class="footer">
		<div class="container">
			<div class="footer-grids">
				<div class="col-md-3 footer-grid animated wow slideInLeft" data-wow-delay=".5s">
					<h3>Tentang Kami</h3>
					<p>Kami adalah penyedia berbagai macam jasa sesuai kebutuhan anda</span></p>
				</div>
				<div class="col-md-3 footer-grid animated wow slideInLeft" data-wow-delay=".6s">
					<h3>Informasi Kontak</h3>
					<ul>
						<li><i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i>Tenggilis Mejoyo, <span>Surabaya.</span></li>
						<li><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i><a href="mailto:info@example.com">jasakuid@gmail.com</a></li>
						<li><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i>081990102911</li>
					</ul>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="footer-logo animated wow slideInUp" data-wow-delay=".5s">
				<h2><a href="{{ url('/index') }}">Jasaku ID</a></h2>
					<ul class="social-icons">
						<li><a href="#" class="facebook"></a></li>
						<li><a href="#" class="twitter"></a></li>
						<li><a href="#" class="g"></a></li>
						<li><a href="#" class="instagram"></a></li>
					</ul>
			</div>
			<div class="copy-right animated wow slideInUp" data-wow-delay=".5s">
				<p>&copy 2018 Jasaku ID. All rights reserved | Design by Team E-Comm Ubaya</p>
			</div>
		</div>
	</div>
<!-- //footer -->
</body>
</html>
<script type="text/javascript" src="{{ asset('js/checkout.js') }}"></script>