<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
<head>
<title>@yield('Title1')</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Best Store Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet" type="text/css" media="all" />
<link href="{{ asset('css/style.css') }}"  rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" type="text/css" href="{{ asset('css/temp.css') }}">
<link rel="icon" href="{{ asset('images/logo_tab.png') }}">
<!-- js -->
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/jquery-ui.min.js') }}"></script>
<script scr="{{ asset('js/jquery-1.12.4.js') }}"></script>
<script src="{{ asset('js/autocomplete.js') }}"></script>
<!-- //js -->
<!-- cart -->
<script src="{{ asset('js/simpleCart.min.js') }}"></script>
<!-- cart -->
<!-- for bootstrap working -->
<script type="text/javascript" src="{{ asset('js/bootstrap-3.1.1.min.js') }}"></script>
<!-- timer -->
<link rel="stylesheet" href="{{ asset('css/jquery.countdown.css') }}" />
<!-- //timer -->
<!-- animation-effect -->
<link href="{{ asset('css/animate.min.css') }}" rel="stylesheet"> 
<link rel="stylesheet" href="{{ asset('css/jquery-ui.css') }}">
<script src="{{ asset('js/wow.min.js') }}"></script>
<script>
 new WOW().init();
</script>
<!-- //animation-effect -->
</head>
	
<body>
<!-- header -->
	<div class="header">
		<div class="container">
			<div class="header-grid">
				<div class="header-grid-left animated wow slideInLeft" data-wow-delay=".5s">
					<ul>
						@if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    	@endif
						@guest
						<li><i class="glyphicon glyphicon-log-in" aria-hidden="true"></i><a href="{{ route('login') }}">Masuk</a></li>
						<li><i class="glyphicon glyphicon-book" aria-hidden="true"></i><a href="{{ route('register') }}">Daftar</a></li>
						@else
						<li><i class="glyphicon glyphicon-log-in" aria-hidden="true"></i><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a></li>
						<form id="logout-form" action="{{ route('logout') }}" method="POST" class="none">
	                                    {{ csrf_field() }}
                        </form>
						<p class="alert alert-success"><a href="{{ url('edit-profil/'.Auth::user()->id.'/edit') }}">{{ Auth::user()->name }}</a></p>
	                    @endguest
					</ul>
				</div>
				<div class="header-grid-right animated wow slideInRight" data-wow-delay=".5s">
					@if(Auth::check())
					<ul class="hitam">
						<div class="simpleCart_shelfItem products-right-grid1-add-cart">
							<p><span class="#"></span><a class="#" href="{{ route('tambah-jasa.index') }}" style="color: black;">Tambah Jasa</a></p>
						</div>
					</ul>
					@endif
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="logo-nav">
				<div class="logo-nav-left animated wow zoomIn" data-wow-delay=".5s">
					<h1><a href="{{ url('/index') }}"><img src="{{ asset('images/logo.png') }}" alt=" " class="img-responsive" /></a></h1>
				</div>
		</div>
	</div>
<!-- //header -->
@yield('Content-Form')
<!-- footer -->
	<div class="footer">
		<div class="container">
			<div class="footer-grids">
				<div class="col-md-3 footer-grid animated wow slideInLeft" data-wow-delay=".5s">
					<h3>Tentang Kami</h3>
					<p>Kami adalah penyedia berbagai macam jasa sesuai kebutuhan anda</span></p>
				</div>
				<div class="col-md-3 footer-grid animated wow slideInLeft" data-wow-delay=".6s">
					<h3>Informasi Kontak</h3>
					<ul>
						<li><i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i>Tenggilis Mejoyo, <span>Surabaya.</span></li>
						<li><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i><a href="mailto:info@example.com">jasakuid@gmail.com</a></li>
						<li><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i>081990102911</li>
					</ul>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="footer-logo animated wow slideInUp" data-wow-delay=".5s">
				<h2><a href="{{ url('/index') }}">Jasaku ID</a></h2>
				<ul class="social-icons">
						<li><a href="#" class="facebook"></a></li>
						<li><a href="#" class="twitter"></a></li>
						<li><a href="#" class="g"></a></li>
						<li><a href="#" class="instagram"></a></li>
					</ul>
			</div>
			<div class="copy-right animated wow slideInUp" data-wow-delay=".5s">
				<p>&copy 2018 Jasaku ID. All rights reserved | Design by Team E-Comm Ubaya</p>
			</div>
		</div>
	</div>
<!-- //footer -->
</body>
</html>
