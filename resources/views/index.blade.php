@extends('layouts.layout')
@section('Title')
	Jasaku ID
@endsection
@section('Content')
<!-- collections -->
	<div class="new-collections">
		<div class="container">
			<h3 class="animated wow zoomIn" data-wow-delay=".5s">Jasa Terbaru</h3>
			<div class="new-collections-grids">
				<div class="col-md-3 new-collections-grid">
					<div class="new-collections-grid1 animated wow slideInUp" data-wow-delay=".5s">
						@foreach($jasasaya as $jasa)
							@if($jasa->id_jasa == 3)
							<div class="new-collections-grid1-image">
								<a href="#" class="product-image"><img src="{{ asset('images/Jasa-user/'.$jasa->gambar_jasa) }}" alt=" " class="img-responsive" /></a>
								<div class="new-collections-grid1-image-pos">
									<a href="#">Quick View</a>
								</div>
							</div>
							<h4><a href="{{ url('detail-jasa/'.$jasa->nama_jasa) }}">{{$jasa->nama_jasa}}</a></h4>
							<p>{{$jasa->deskripsi_jasa}}</p>
							<h4 class="fontjasa">Rp {{ number_format($jasa->harga_jasa, 2) }}</h4>
							<div class="simpleCart_shelfItem products-right-grid1-add-cart">
								<p><a class="item_add mid" href="{{ url('/cart/'.$jasa->id_jasa) }}">Pilih </a></p>
							</div>
							@endif
						@endforeach
					</div>					
				</div>
				<div class="col-md-6 new-collections-grid">
					<!-- Untuk Jasa Terbaru Disini - SEPTIANO -->
					<div class="new-collections-grid1 new-collections-grid1-image-width animated wow slideInUp" data-wow-delay=".5s">
							<div class="new-collections-grid1-image">
								<a href="#" class="product-image"><img src="{{ asset('images/Jasa-user/'.$jasaTerbaru->gambar_jasa) }}" alt=" " class="img-responsive" /></a>
								<div class="new-collections-grid1-image-pos new-collections-grid1-image-pos1">
									<a href="#">Quick View</a>
								</div>
								<div class="new-one">
									<p>Baru</p>
								</div>
							</div>
							<h4><a href="#">{{ $jasaTerbaru->nama_jasa }}</a></h4>
							<p>{{$jasaTerbaru->deskripsi}}</p>
							<h4 class="fontjasa">Rp {{ number_format($jasaTerbaru->harga_jasa, 2) }}</h4>
							<div class="simpleCart_shelfItem products-right-grid1-add-cart">
								<p><a class="item_add mid" href="{{ url('/cart/'.$jasa->id_jasa) }}">Pilih </a></p>
							</div>
					</div>					
				<!-- //Untuk Jasa Terbaru Disini - SEPTIANO -->
					<div class="new-collections-grid-sub-grids">
						<div class="new-collections-grid1-sub">
							<div class="new-collections-grid1 animated wow slideInUp" data-wow-delay=".5s">
								@foreach($jasasaya as $jasa)
									@if($jasa->id_jasa == 4)
									<div class="new-collections-grid1-image">
										<a href="#" class="product-image"><img src="{{ asset('images/Jasa-user/'.$jasa->gambar_jasa) }}" alt=" " class="img-responsive" /></a>
										<div class="new-collections-grid1-image-pos">
											<a href="#">Quick View</a>
										</div>
									</div>
									<h4><a href="#">{{$jasa->nama_jasa}}</a></h4>
									<p>{{$jasa->deskripsi_jasa}}</p>
									<h4 class="fontjasa">Rp {{ number_format($jasa->harga_jasa, 2) }}</h4>
									<div class="simpleCart_shelfItem products-right-grid1-add-cart">
										<p><a class="item_add mid" href="{{ url('/cart/'.$jasa->id_jasa) }}">Pilih </a></p>
									</div>
									@endif
								@endforeach
							</div>
						</div>
						<div class="new-collections-grid1-sub">
							<div class="new-collections-grid1 animated wow slideInUp" data-wow-delay=".5s">
								@foreach($jasasaya as $jasa)
									@if($jasa->id_jasa == 5)
									<div class="new-collections-grid1-image">
										<a href="#" class="product-image"><img src="{{ asset('images/Jasa-user/'.$jasa->gambar_jasa) }}" alt=" " class="img-responsive" /></a>
										<div class="new-collections-grid1-image-pos">
											<a href="#">Quick View</a>
										</div>
									</div>
									<h4><a href="#">{{$jasa->nama_jasa}}</a></h4>
									<p>{{$jasa->deskripsi_jasa}}</p>
									<h4 class="fontjasa">Rp {{ number_format($jasa->harga_jasa, 2) }}</h4>
									<div class="simpleCart_shelfItem products-right-grid1-add-cart">
										<p><a class="item_add mid" href="{{ url('/cart/'.$jasa->id_jasa) }}">Pilih </a></p>
									</div>
									@endif
								@endforeach
							</div>
						</div>
						<div class="clearfix"> </div>
					</div>
				</div>

				<div class="col-md-3 new-collections-grid">
					<div class="new-collections-grid1 animated wow slideInUp" data-wow-delay=".5s">
						@foreach($jasasaya as $jasa)
							@if($jasa->id_jasa == 2)
							<div class="new-collections-grid1-image">
								<a href="#" class="product-image"><img src="{{ asset('images/Jasa-user/'.$jasa->gambar_jasa) }}" alt=" " class="img-responsive" /></a>
								<div class="new-collections-grid1-image-pos">
									<a href="#">Quick View</a>
								</div>
							</div>
							<h4><a href="#">{{$jasa->nama_jasa}}</a></h4>
							<p>{{$jasa->deskripsi_jasa}}</p>
							<h4 class="fontjasa">Rp {{ number_format($jasa->harga_jasa, 2) }}</h4>
							<div class="simpleCart_shelfItem products-right-grid1-add-cart">
								<p><a class="item_add mid" href="{{ url('/cart/'.$jasa->id_jasa) }}">Pilih </a></p>
							</div>
							@endif
						@endforeach
					</div>
				</div>
				
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
<!-- //collections -->
@endsection