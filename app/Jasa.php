<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jasa extends Model
{
	protected $fillable = [
		'nama_jasa', 'deskripsi', 'gambar_jasa', 'user_id', 'kategori_jasa_id', 'harga_jasa'
	];

    public function scopeSearch($query, $s){
    	return $query->where('nama_jasa', 'like', '%'.$s.'%');	
    }
}
