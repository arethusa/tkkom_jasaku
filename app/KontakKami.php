<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KontakKami extends Model
{
    protected $fillable = [
        'nama', 'email', 'subject', 'pesan',
    ];
}
