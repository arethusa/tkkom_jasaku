<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model
{
    protected $fillable = [
        'nama', 'email', 'alamat', 'rating', 'judul_testi', 'pesan_testi', 
    ];
}
