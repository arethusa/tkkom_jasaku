<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pembayaran extends Model
{
    protected $fillable = [
        'nomor_pembayaran', 'jasa_yang_disewa', 'total_pembayaran', 'nama_pemesan', 'alamat', 'nama_pemilik_rekening', 'nomor_rekening', 'bukti_pembayaran', 'qrcode'
    ];
}
