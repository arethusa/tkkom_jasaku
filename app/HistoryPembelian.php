<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoryPembelian extends Model
{
    protected $fillable = [
		'user_id', 'jasa_id', 'qty', 'totalPrice'
	];
}
