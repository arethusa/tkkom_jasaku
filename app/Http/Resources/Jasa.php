<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Jasa extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'nama_jasa' => $this->nama_jasa,
            'deskripsi' => $this->deskripsi,
            'gambar_jasa' => $this->gambar_jasa,
            'user_id' => $this->user_id,
            'kategori_jasa_id' => $this->kategori_jasa_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'harga_jasa' => $this->harga_jasa,
        ];
    }
}
