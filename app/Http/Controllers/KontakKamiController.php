<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
use App\User;
use Validator;
use App\Kategori;
use App\Jasa;
use App\KontakKami;

class KontakKamiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::check()) 
        {
            $kategori = Kategori::all();
            $jasasaya = DB::table('kategori_jasas')
                                            ->join('jasas', 'kategori_jasas.id', '=', 'jasas.kategori_jasa_id')
                                            ->select('kategori_jasas.nama_kategori_jasa as nama_kategori',
                                                'kategori_jasas.id as kategori_id',
                                                'jasas.id as id_jasa', 
                                                'jasas.nama_jasa as nama_jasa', 
                                                'jasas.gambar_jasa as gambar_jasa', 
                                                'jasas.harga_jasa as harga_jasa',
                                                'jasas.deskripsi as deskripsi_jasa', 
                                                'jasas.kategori_jasa_id as kategori_jasa_id')
                                            ->get();
            $jasaTerbaru = Jasa::find(DB::table('jasas')->max('id'));
            return view('jasaku.kontakkami', ['jasasaya' => $jasasaya, 'kategori' => $kategori, 'jasaTerbaru' => $jasaTerbaru]);
        }
        else
        {
            $kategori = Kategori::all();
            $jasasaya = DB::table('kategori_jasas')
                                            ->join('jasas', 'kategori_jasas.id', '=', 'jasas.kategori_jasa_id')
                                            ->select('kategori_jasas.nama_kategori_jasa as nama_kategori',
                                                'kategori_jasas.id as kategori_id',
                                                'jasas.id as id_jasa', 
                                                'jasas.nama_jasa as nama_jasa', 
                                                'jasas.gambar_jasa as gambar_jasa', 
                                                'jasas.harga_jasa as harga_jasa',
                                                'jasas.deskripsi as deskripsi_jasa', 
                                                'jasas.kategori_jasa_id as kategori_jasa_id')
                                            ->get();
            $jasaTerbaru = Jasa::find(DB::table('jasas')->max('id'));
            return view('jasaku.kontakkami', ['jasasaya' => $jasasaya, 'kategori' => $kategori, 'jasaTerbaru' => $jasaTerbaru]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $kontak_kami = new KontakKami(
            array(
                'nama' => $request->get('nama'),
                'email' => $request->get('email'),
                'subject' => $request->get('subject'),
                'pesan' => $request->get('pesan'),
            ));
        $kontak_kami->save();
        return redirect('kontak-kami')->with('status', 'Pesan anda dengan subject '.$request->get('subject').' berhasil dilampirkan kepada Admin Jasaku. Tunggu kabar selanjutnya!');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
