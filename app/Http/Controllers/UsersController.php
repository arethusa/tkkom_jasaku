<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
use App\User;
use Validator;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::check()) 
        {
            /*$id_User = Auth::user()->id;
            //var_dump($id_User);
            $profil = User::*/
            return view('jasaku.editprofile');
        }
        else
        {

            return view('jasaku.editprofile');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Auth::check()) 
        {
            $id = Auth::user()->id;
            $profil = User::whereId($id)->firstOrFail();
            return view('jasaku.editprofile', compact('profil'));
        }
        else
        {

            return redirect('login')->with('status', 'Anda Harus Login Terlebih Dahulu');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $profil = User::whereId($id)->firstOrFail();
        $profil->name = $request->get('name');
        $profil->username = $request->get('username');
        $profil->email = $request->get('email');
        $profil->no_handphone = $request->get('no_handphone');
        $profil->save();

        return redirect('index')->with('status', 'Data Profil '.$profil->name.' Telah Diperbaharui');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
