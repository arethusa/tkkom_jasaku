<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kategori;
use App\Jasa;
use DB;
use Illuminate\Support\Facades\Auth;

class KategorisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*$kategori = Kategori::all();
        return view('jasaku.kategori-jasa', compact('kategori'));*/
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(Auth::check())
        {
            $kategori = Kategori::all();           
            $kategoriBerdasarkanId = DB::table('kategori_jasas')
                                            ->join('jasas', 'kategori_jasas.id', '=', 'jasas.kategori_jasa_id')
                                            ->select('kategori_jasas.nama_kategori_jasa as nama_kategori',
                                                'kategori_jasas.id as kategori_id',
                                                'jasas.id as id_jasa', 
                                                'jasas.nama_jasa as nama_jasa',
                                                'jasas.deskripsi as deskripsi', 
                                                'jasas.gambar_jasa as gambar_jasa', 
                                                'jasas.harga_jasa as harga_jasa',
                                                'jasas.kategori_jasa_id as kategori_jasa_id')
                                            ->where('kategori_jasas.nama_kategori_jasa', $id)
                                            ->get();
            $jasasaya = DB::table('kategori_jasas')
                                            ->join('jasas', 'kategori_jasas.id', '=', 'jasas.kategori_jasa_id')
                                            ->select('kategori_jasas.nama_kategori_jasa as nama_kategori',
                                                'kategori_jasas.id as kategori_id',
                                                'jasas.id as id_jasa', 
                                                'jasas.nama_jasa as nama_jasa', 
                                                'jasas.gambar_jasa as gambar_jasa', 
                                                'jasas.harga_jasa as harga_jasa',
                                                'jasas.deskripsi as deskripsi_jasa', 
                                                'jasas.kategori_jasa_id as kategori_jasa_id')
                                            ->get();
            return view('jasaku.kategori-jasa', ['kategori' => $kategori,'kategoriBerdasarkanId' => $kategoriBerdasarkanId, 'jasasaya' => $jasasaya]);
        }
        else
        {
            $kategori = Kategori::all();           
            $kategoriBerdasarkanId = DB::table('kategori_jasas')
                                            ->join('jasas', 'kategori_jasas.id', '=', 'jasas.kategori_jasa_id')
                                            ->select('kategori_jasas.nama_kategori_jasa as nama_kategori',
                                                'kategori_jasas.id as kategori_id',
                                                'jasas.id as id_jasa', 
                                                'jasas.nama_jasa as nama_jasa',
                                                'jasas.deskripsi as deskripsi', 
                                                'jasas.gambar_jasa as gambar_jasa', 
                                                'jasas.harga_jasa as harga_jasa',
                                                'jasas.kategori_jasa_id as kategori_jasa_id')
                                            ->where('kategori_jasas.nama_kategori_jasa', $id)
                                            ->get();
            $jasasaya = DB::table('kategori_jasas')
                                            ->join('jasas', 'kategori_jasas.id', '=', 'jasas.kategori_jasa_id')
                                            ->select('kategori_jasas.nama_kategori_jasa as nama_kategori',
                                                'kategori_jasas.id as kategori_id',
                                                'jasas.id as id_jasa', 
                                                'jasas.nama_jasa as nama_jasa', 
                                                'jasas.gambar_jasa as gambar_jasa', 
                                                'jasas.harga_jasa as harga_jasa',
                                                'jasas.deskripsi as deskripsi_jasa', 
                                                'jasas.kategori_jasa_id as kategori_jasa_id')
                                            ->get();
            return view('jasaku.kategori-jasa', ['kategori' => $kategori,'kategoriBerdasarkanId' => $kategoriBerdasarkanId, 'jasasaya' => $jasasaya]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
