<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
Use App\Jasa;
use App\Http\Resources\Jasa as JasaResource;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Pagination\Paginator;
use Illuminate\Contracts\Support\Jsonable;

class DaftarJasasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jasa = Jasa::all();
        return view('admin.daftar-jasa', compact('jasa'));
    }

    public function jsonJasa()
    {
        $jasa = Jasa::paginate();
        // return response()->json($jasa);
        if(isset($jasa)){
            foreach ($jasa as $kk) {

                echo "<pre> '".json_encode($jasa, JSON_PRETTY_PRINT)."' </pre>";
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $jasa = Jasa::whereId($id)->firstOrFail();
        $namaJasa = $jasa->nama_jasa;
        $jasa->delete();

        return back()->with('status', 'User dengan nama '.$namaJasa.' berhasil dihapus!');
    }
}
