<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
use App\Kategori;
use App\Jasa;
use Illuminate\Http\UploadedFile;
use Image;
use Input;
use Validator;
use Response;

class TambahJasaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::check())
        {
            if(auth()->user()->verified() == true)
            {
                $kategori = DB::table('jasas as j')
                    ->join('kategori_jasas as k', 'j.id', '=', 'k.id')
                    ->select('k.nama_kategori_jasa as nama', 'k.id as id')
                    ->get();
                return view('jasaku.tambah-jasa', compact('kategori'));
            }
            else
                return redirect('index')->with('status', 'Harap verfikasi akun Anda terlebih dahulu');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->hasFile('gambar_jasa')){
            error_reporting(E_ALL);
            ini_set('memory_limit', '-1');
            $gambar_jasa = $request->file('gambar_jasa');
            $filename = $gambar_jasa->getClientOriginalName();

            Image::make($gambar_jasa)->save(public_path('images/Jasa-user/'.$filename));
            $harga = $request->get('harga_jasa');
            $hargaBaru = preg_replace('/\D/', '', $harga)/100;

            $jasa = new Jasa(
                array(
                    'nama_jasa' => $request->get('nama_jasa'),
                    'deskripsi' => $request->get('deskripsi'),
                    'gambar_jasa' => $filename,
                    'harga_jasa' => $hargaBaru,
                    'user_id' => Auth::user()->id,
                    'kategori_jasa_id' => $request->get('user_kategori'),
                ));
            $jasa->save();
            return redirect('index')->with('status', 'Jasa dengan nama '.$request->get('nama_jasa').' berhasil ditambahkan');
        }
        else{
             return redirect('jasaku.tambah-jasa')->with('status', 'Mohon periksa kembali data-data yang diminta');
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
