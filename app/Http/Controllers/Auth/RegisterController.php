<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/index';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $messages = [
            'name.required' => 'Nama Harus Diisi',
            'username.required' => 'Username Harus Diisi',
            'username.unique' => 'Username Sudah Ada',
            'no_handphone.required' => 'No Handphone Harus Diisi',
            'no_handphone.min' => 'No Handphone Minimal :min Digit',
            'email.required' => 'Email Harus Diisi',
            'email.unique' => 'Email Sudah Ada', 
            'password.min' => 'Kata Sandi Minimal :min Karakter',
            'password.confirmed' => 'Konfirmasi Kata Sandi Tidak Sama'
        ];

        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'username' => 'required|string|max:255|unique:users',
            'no_handphone' => 'required|string|min:10',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ], $messages);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $penjual_pembeli = "penjual-pembeli";
        $user =  User::create([
            'name' => $data['name'],
            'username' => $data['username'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'status' => $penjual_pembeli,
            'no_handphone' => $data['no_handphone'],
            'token' => str_random(25),
        ]);
        //'password' => iconv("UTF-8", "UTF-8//IGNORE", base64_encode($data['password'])),
        $user->sendVerificationEmail();
        return $user;
    }
}
