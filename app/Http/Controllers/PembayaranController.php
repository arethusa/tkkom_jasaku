<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
use App\Cart;
use App\Jasa;
use App\Kategori;
use App\HistoryPembelian;
use Session;
use App\Pembayaran;
use Illuminate\Http\UploadedFile;
use Image;
use Input;
use Validator;
use Response;
use QRCode;

class PembayaranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        function __construct()
        {
            $this->middleware('auth');
        }
        if(Auth::check())
        {
            if(auth()->user()->verified() == true)
            {
                if(!Session::has('cart'))
                {
                    $kategori = Kategori::all();
                    $jasasaya = DB::table('kategori_jasas')
                                                    ->join('jasas', 'kategori_jasas.id', '=', 'jasas.kategori_jasa_id')
                                                    ->select('kategori_jasas.nama_kategori_jasa as nama_kategori',
                                                        'kategori_jasas.id as kategori_id',
                                                        'jasas.id as id_jasa', 
                                                        'jasas.nama_jasa as nama_jasa', 
                                                        'jasas.gambar_jasa as gambar_jasa', 
                                                        'jasas.harga_jasa as harga_jasa',
                                                        'jasas.deskripsi as deskripsi_jasa', 
                                                        'jasas.kategori_jasa_id as kategori_jasa_id')
                                                    ->get();
                    return view('jasaku.pembayaran', ['jasas' => null, 'jasasaya' => $jasasaya, 'kategori' => $kategori]);
                }
                else
                {
                    $oldCart = Session::get('cart');
                    $cart = new Cart($oldCart);

                    $kategori = Kategori::all();
                    $jasasaya = DB::table('kategori_jasas')
                                                    ->join('jasas', 'kategori_jasas.id', '=', 'jasas.kategori_jasa_id')
                                                    ->select('kategori_jasas.nama_kategori_jasa as nama_kategori',
                                                        'kategori_jasas.id as kategori_id',
                                                        'jasas.id as id_jasa', 
                                                        'jasas.nama_jasa as nama_jasa', 
                                                        'jasas.gambar_jasa as gambar_jasa', 
                                                        'jasas.harga_jasa as harga_jasa',
                                                        'jasas.deskripsi as deskripsi_jasa', 
                                                        'jasas.kategori_jasa_id as kategori_jasa_id')
                                                    ->get();
                    $jasaTerbaru = Jasa::find(DB::table('jasas')->max('id'));
                    $jasaYangSesuai = DB::table('kategori_jasas')
                                                    ->join('jasas', 'kategori_jasas.id', '=', 'jasas.kategori_jasa_id')
                                                    ->select('kategori_jasas.nama_kategori_jasa as nama_kategori',
                                                        'kategori_jasas.id as kategori_id',
                                                        'jasas.id as id_jasa', 
                                                        'jasas.nama_jasa as nama_jasa', 
                                                        'jasas.gambar_jasa as gambar_jasa', 
                                                        'jasas.harga_jasa as harga_jasa',
                                                        'jasas.kategori_jasa_id as kategori_jasa_id')
                                                    ->get();                                  
                    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                    $charactersLength = strlen($characters);
                    $randomString = '';
                    $randomNum = rand(10000,99999);
                    $dateNow = date("YmdHis");
                    for ($i = 0; $i < 4; $i++) {
                        $randomString .= $characters[rand(0, $charactersLength - 1)];
                    }
                    return view('jasaku.pembayaran', ['jasas' => $cart->items, 'totalPrice' => $cart->totalPrice, 'jasasaya' => $jasasaya, 'kategori' => $kategori, 'jasaTerbaru' => $jasaTerbaru, 'jasaYangSesuai' => $jasaYangSesuai, 'randomString' => $randomString, 'randomNum' => $randomNum, 'dateNow' => $dateNow]);
                }
            }
            else
                return redirect('index')->with('status', 'Harap verfikasi akun Anda terlebih dahulu');
        }
        else
        {
            return redirect('login')->with('status', 'Anda Harus Login Untuk Melanjutkan');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function rc4($key, $str) {
        //buat S-Mod
        $s = array();
        for ($i = 0; $i < 256; $i++) {
            $s[$i] = $i;
        }

        //set nilai j 0-255 
        //value j ditukar dengan nilai S-Box 
        $j = 0;
        for ($i = 0; $i < 256; $i++) {
            $j = ($j + $s[$i] + ord($key[$i % strlen($key)])) % 256;
            $x = $s[$i];
            $s[$i] = $s[$j];
            $s[$j] = $x;
        }

        //membuat pseudorandom key berdasarkan indeks dan nilai S-Box 
        $i = 0;
        $j = 0;
        $res = '';
        for ($y = 0; $y < strlen($str); $y++) {
            $i = ($i + 1) % 256;
            $j = ($j + $s[$i]) % 256;
            $x = $s[$i];
            $s[$i] = $s[$j];
            $s[$j] = $x;
            $res .= $str[$y] ^ chr($s[($s[$i] + $s[$j]) % 256]);
        }
        return $res;
    }

    public function store(Request $request)
    {
        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);

        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        $randomNum = rand(10000,99999);
        $dateNow = date("YmdHis");
        for ($i = 0; $i < 4; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        if($request->hasFile('bukti_pembayaran')){
            error_reporting(E_ALL);
            ini_set('memory_limit', '-1');
            $bukti_pembayaran = $request->file('bukti_pembayaran');
            $filename = $bukti_pembayaran->getClientOriginalName();

            $nomorpembayaranuser = $dateNow.$randomNum.$randomString;

            Image::make($bukti_pembayaran)->save(public_path('images/Pembayaran-user/'.$filename));

            $path = Auth::user()->name.$nomorpembayaranuser.'.png';
            $file = public_path('/qr-code/'.$path);
            $qrtext = QRCode::text($nomorpembayaranuser)->setOutfile($file)->setErrorCorrectionLevel('H')->setSize(4)->setMargin(3)->png();    
            
            //nomor pembayaran menggunakan encryption rc4
            $pembayaran = new Pembayaran(
                array(
                    'nomor_pembayaran' => iconv("UTF-8", "UTF-8//IGNORE", $this->rc4('jasaku', $nomorpembayaranuser)),
                    'jasa_yang_disewa' => $cart->namaItem,
                    'total_pembayaran' => $cart->totalPrice,
                    'nama_pemesan' => Auth::user()->name,
                    'alamat' => $request->get('alamat'),
                    'nama_pemilik_rekening' => $request->get('nama_pemilik_rekening'),
                    'nomor_rekening' => $request->get('nomor_rekening'),
                    'bukti_pembayaran' => $filename,
                    'qrcode' => $path,
                ));
            // dd($pembayaran);
            $pembayaran->save();
            // dd($path);
            return redirect('history')->with('status', 'Jasa dengan nama '.$request->get('nama_jasa').' berhasil ditambahkan');
        }
        else{
             return redirect('pembayaran-jasa')->with('status', 'Mohon periksa kembali data-data yang diminta');
        }   
        // $cart->items = collect($cart->items)->implode(':');
        // foreach($cart as $carts)
        // {
        //     return $carts['items'];
        // }
        // dd($oldCart);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
