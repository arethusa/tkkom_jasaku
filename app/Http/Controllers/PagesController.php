<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
use App\Cart;
use App\Jasa;
use App\Kategori;
use App\HistoryPembelian;
use Session;

class PagesController extends Controller
{
	public function index(Request $request)
    {
        if (Auth::check()) 
        {
            $kategori = Kategori::all();
            $jasasaya = DB::table('kategori_jasas')
                                            ->join('jasas', 'kategori_jasas.id', '=', 'jasas.kategori_jasa_id')
                                            ->select('kategori_jasas.nama_kategori_jasa as nama_kategori',
                                                'kategori_jasas.id as kategori_id',
                                                'jasas.id as id_jasa', 
                                                'jasas.nama_jasa as nama_jasa', 
                                                'jasas.gambar_jasa as gambar_jasa', 
                                                'jasas.harga_jasa as harga_jasa',
                                                'jasas.deskripsi as deskripsi_jasa', 
                                                'jasas.kategori_jasa_id as kategori_jasa_id')
                                            ->get();
            $jasaTerbaru = Jasa::find(DB::table('jasas')->max('id'));
            return view('index', ['jasasaya' => $jasasaya, 'kategori' => $kategori, 'jasaTerbaru' => $jasaTerbaru]);
        }
        else
        {
            $kategori = Kategori::all();
            $jasasaya = DB::table('kategori_jasas')
                                            ->join('jasas', 'kategori_jasas.id', '=', 'jasas.kategori_jasa_id')
                                            ->select('kategori_jasas.nama_kategori_jasa as nama_kategori',
                                                'kategori_jasas.id as kategori_id',
                                                'jasas.id as id_jasa', 
                                                'jasas.nama_jasa as nama_jasa', 
                                                'jasas.gambar_jasa as gambar_jasa', 
                                                'jasas.harga_jasa as harga_jasa',
                                                'jasas.deskripsi as deskripsi_jasa', 
                                                'jasas.kategori_jasa_id as kategori_jasa_id')
                                            ->get();
            $jasaTerbaru = Jasa::find(DB::table('jasas')->max('id'));
            return view('index', ['jasasaya' => $jasasaya, 'kategori' => $kategori, 'jasaTerbaru' => $jasaTerbaru]);
        }
    }

    public function search(Request $request)
    {
        if (Auth::check()) 
        {
            // The user is logged in...
            //return view('index');
            $item = $request->term;
            $search = DB::table('jasas')->where('nama_jasa', 'LIKE', '%'.$item.'%')->get();

            if(count($search) == 0){
                $res[] = 'Pencarian tidak ditemukan';
            } else{
                foreach ($search as $key => $value) {
                    $res[] = $value->nama_jasa;
                }
            }
            return $res;
        }
        else
        {
            //return redirect('login')->with('status', 'Anda Harus Login Terlebih Dahulu!');
            //return view ('index', ['jasasaya' => $jasa]);
            $item = $request->term;
            $search = DB::table('jasas')->where('nama_jasa', 'LIKE', '%'.$item.'%')->get();

            if(count($search) == 0){
                $res[] = 'Pencarian tidak ditemukan';
            } else{
                foreach ($search as $key => $value) {
                    $res[] = $value->nama_jasa;
                }
            }
            return $res;
        }
    }

    public function hasilPencarian(Request $request)
    {
        if(Auth::check())
        {
            $s = $request->input('searchrow');
            $jasasayaSearch = Jasa::latest()
                        ->search($s)
                        ->paginate(8);
            if($jasasayaSearch[0] != NULL)
            {
                $kategori = Kategori::all();
                $jasasaya = DB::table('kategori_jasas')
                                ->join('jasas', 'kategori_jasas.id', '=', 'jasas.kategori_jasa_id')
                                ->select('kategori_jasas.nama_kategori_jasa as nama_kategori',
                                    'kategori_jasas.id as kategori_id',
                                    'jasas.id as id_jasa', 
                                    'jasas.nama_jasa as nama_jasa', 
                                    'jasas.gambar_jasa as gambar_jasa', 
                                    'jasas.harga_jasa as harga_jasa',
                                    'jasas.deskripsi as deskripsi_jasa', 
                                    'jasas.kategori_jasa_id as kategori_jasa_id')
                                ->get();
                $echo = "Ada";
                return view('hasilPencarian', compact('jasasayaSearch', 's', 'kategori', 'jasasaya', 'echo'));
            }
            else{
                $kategori = Kategori::all();
                $jasasaya = DB::table('kategori_jasas')
                                ->join('jasas', 'kategori_jasas.id', '=', 'jasas.kategori_jasa_id')
                                ->select('kategori_jasas.nama_kategori_jasa as nama_kategori',
                                    'kategori_jasas.id as kategori_id',
                                    'jasas.id as id_jasa', 
                                    'jasas.nama_jasa as nama_jasa', 
                                    'jasas.gambar_jasa as gambar_jasa', 
                                    'jasas.harga_jasa as harga_jasa',
                                    'jasas.deskripsi as deskripsi_jasa', 
                                    'jasas.kategori_jasa_id as kategori_jasa_id')
                                ->get();
                $echo = "Pencarian tidak ditemukan";
                return view('hasilPencarian', compact('jasasayaSearch', 's', 'kategori', 'jasasaya', 'echo'));
            }
        }
        else
        {
            $s = $request->input('searchrow');
            $jasasayaSearch = Jasa::latest()
                        ->search($s)
                        ->paginate(8);
            if($jasasayaSearch[0] != NULL)
            {
                $kategori = Kategori::all();
                $jasasaya = DB::table('kategori_jasas')
                                ->join('jasas', 'kategori_jasas.id', '=', 'jasas.kategori_jasa_id')
                                ->select('kategori_jasas.nama_kategori_jasa as nama_kategori',
                                    'kategori_jasas.id as kategori_id',
                                    'jasas.id as id_jasa', 
                                    'jasas.nama_jasa as nama_jasa', 
                                    'jasas.gambar_jasa as gambar_jasa', 
                                    'jasas.harga_jasa as harga_jasa',
                                    'jasas.deskripsi as deskripsi_jasa', 
                                    'jasas.kategori_jasa_id as kategori_jasa_id')
                                ->get();
                $echo = "Ada";
                return view('hasilPencarian', compact('jasasayaSearch', 's', 'kategori', 'jasasaya', 'echo'));
            }
            else{
                $kategori = Kategori::all();
                $jasasaya = DB::table('kategori_jasas')
                                ->join('jasas', 'kategori_jasas.id', '=', 'jasas.kategori_jasa_id')
                                ->select('kategori_jasas.nama_kategori_jasa as nama_kategori',
                                    'kategori_jasas.id as kategori_id',
                                    'jasas.id as id_jasa', 
                                    'jasas.nama_jasa as nama_jasa', 
                                    'jasas.gambar_jasa as gambar_jasa', 
                                    'jasas.harga_jasa as harga_jasa',
                                    'jasas.deskripsi as deskripsi_jasa', 
                                    'jasas.kategori_jasa_id as kategori_jasa_id')
                                ->get();
                $echo = "Pencarian tidak ditemukan";
                return view('hasilPencarian', compact('jasasayaSearch', 's', 'kategori', 'jasasaya', 'echo'));
            }
        }
    }

    public function produk()
    {
        return view('jasaku.produk');
    }

    public function detailJasa($idJasa)
    {
        if (Auth::check())
        {
            $jasasaya = DB::table('kategori_jasas')
                                            ->join('jasas', 'kategori_jasas.id', '=', 'jasas.kategori_jasa_id')
                                            ->select('kategori_jasas.nama_kategori_jasa as nama_kategori',
                                                'kategori_jasas.id as kategori_id',
                                                'jasas.id as id_jasa', 
                                                'jasas.nama_jasa as nama_jasa', 
                                                'jasas.gambar_jasa as gambar_jasa', 
                                                'jasas.harga_jasa as harga_jasa',
                                                'jasas.deskripsi as deskripsi_jasa', 
                                                'jasas.kategori_jasa_id as kategori_jasa_id')
                                            ->get();
            $kategori = Kategori::all(); 
            $detailJasa = DB::table('jasas')->where('jasas.nama_jasa', $idJasa)->get();
            $jasaYangSesuai = DB::table('kategori_jasas')
                                            ->join('jasas', 'kategori_jasas.id', '=', 'jasas.kategori_jasa_id')
                                            ->select('kategori_jasas.nama_kategori_jasa as nama_kategori',
                                                'kategori_jasas.id as kategori_id',
                                                'jasas.id as id_jasa', 
                                                'jasas.nama_jasa as nama_jasa', 
                                                'jasas.gambar_jasa as gambar_jasa', 
                                                'jasas.harga_jasa as harga_jasa',
                                                'jasas.kategori_jasa_id as kategori_jasa_id')
                                            ->where('jasas.nama_jasa', $idJasa)
                                            ->get();
            return view('jasaku.detail-jasa', ['detailJasa' => $detailJasa, 'jasasaya' => $jasasaya, 'jasaYangSesuai' => $jasaYangSesuai, 'kategori' => $kategori]);
        }
        else
        {
            $jasasaya = DB::table('kategori_jasas')
                                            ->join('jasas', 'kategori_jasas.id', '=', 'jasas.kategori_jasa_id')
                                            ->select('kategori_jasas.nama_kategori_jasa as nama_kategori',
                                                'kategori_jasas.id as kategori_id',
                                                'jasas.id as id_jasa', 
                                                'jasas.nama_jasa as nama_jasa', 
                                                'jasas.gambar_jasa as gambar_jasa', 
                                                'jasas.harga_jasa as harga_jasa',
                                                'jasas.deskripsi as deskripsi_jasa', 
                                                'jasas.kategori_jasa_id as kategori_jasa_id')
                                            ->get();
            $kategori = Kategori::all(); 
            $detailJasa = DB::table('jasas')->where('jasas.nama_jasa', $idJasa)->get();
            $jasaYangSesuai = DB::table('kategori_jasas')
                                            ->join('jasas', 'kategori_jasas.id', '=', 'jasas.kategori_jasa_id')
                                            ->select('kategori_jasas.nama_kategori_jasa as nama_kategori',
                                                'kategori_jasas.id as kategori_id',
                                                'jasas.id as id_jasa', 
                                                'jasas.nama_jasa as nama_jasa', 
                                                'jasas.gambar_jasa as gambar_jasa', 
                                                'jasas.harga_jasa as harga_jasa',
                                                'jasas.kategori_jasa_id as kategori_jasa_id')
                                            ->where('jasas.nama_jasa', $idJasa)
                                            ->get();
            return view('jasaku.detail-jasa', ['detailJasa' => $detailJasa, 'jasasaya' => $jasasaya, 'jasaYangSesuai' => $jasaYangSesuai, 'kategori' => $kategori]);
        } 
    }

    public function AddToCart(Request $request, $id)
    {
        function __construct()
        {
            $this->middleware('auth');
        }
        if(Auth::check())
        {
            if(auth()->user()->verified() == true)
            {
                $jasa = Jasa::find($id);
                $oldCart = Session::has('cart') ? Session::get('cart') : null;
                $cart = new Cart($oldCart);
                $cart->add($jasa, $jasa->id);

                $request->session()->put('cart', $cart);
                // dd($request->session()->get('cart'));
                return redirect()->back();
            }
            else
                return redirect('index')->with('status', 'Harap verfikasi akun Anda terlebih dahulu');
        }
        else
        {
            return redirect('login')->with('status', 'Anda Harus Login Untuk Melanjutkan');
        }
    }

    public function cart()
    {
        function __construct()
        {
            $this->middleware('auth');
        }
        if(Auth::check())
        {
            if(auth()->user()->verified() == true)
            {
                if(!Session::has('cart'))
                {
                    $kategori = Kategori::all();
                    $jasasaya = DB::table('kategori_jasas')
                                                    ->join('jasas', 'kategori_jasas.id', '=', 'jasas.kategori_jasa_id')
                                                    ->select('kategori_jasas.nama_kategori_jasa as nama_kategori',
                                                        'kategori_jasas.id as kategori_id',
                                                        'jasas.id as id_jasa', 
                                                        'jasas.nama_jasa as nama_jasa', 
                                                        'jasas.gambar_jasa as gambar_jasa', 
                                                        'jasas.harga_jasa as harga_jasa',
                                                        'jasas.deskripsi as deskripsi_jasa', 
                                                        'jasas.kategori_jasa_id as kategori_jasa_id')
                                                    ->get();
                    return view('jasaku.keranjang', ['jasas' => null, 'jasasaya' => $jasasaya, 'kategori' => $kategori]);
                }
                else
                {
                    $oldCart = Session::get('cart');
                    $cart = new Cart($oldCart);

                    $kategori = Kategori::all();
                    $jasasaya = DB::table('kategori_jasas')
                                                    ->join('jasas', 'kategori_jasas.id', '=', 'jasas.kategori_jasa_id')
                                                    ->select('kategori_jasas.nama_kategori_jasa as nama_kategori',
                                                        'kategori_jasas.id as kategori_id',
                                                        'jasas.id as id_jasa', 
                                                        'jasas.nama_jasa as nama_jasa', 
                                                        'jasas.gambar_jasa as gambar_jasa', 
                                                        'jasas.harga_jasa as harga_jasa',
                                                        'jasas.deskripsi as deskripsi_jasa', 
                                                        'jasas.kategori_jasa_id as kategori_jasa_id')
                                                    ->get();
                    $jasaTerbaru = Jasa::find(DB::table('jasas')->max('id'));
                    $jasaYangSesuai = DB::table('kategori_jasas')
                                                    ->join('jasas', 'kategori_jasas.id', '=', 'jasas.kategori_jasa_id')
                                                    ->select('kategori_jasas.nama_kategori_jasa as nama_kategori',
                                                        'kategori_jasas.id as kategori_id',
                                                        'jasas.id as id_jasa', 
                                                        'jasas.nama_jasa as nama_jasa', 
                                                        'jasas.gambar_jasa as gambar_jasa', 
                                                        'jasas.harga_jasa as harga_jasa',
                                                        'jasas.kategori_jasa_id as kategori_jasa_id')
                                                    ->get();                                  
                    return view('jasaku.keranjang', ['jasas' => $cart->items, 'totalPrice' => $cart->totalPrice, 'jasasaya' => $jasasaya, 'kategori' => $kategori, 'jasaTerbaru' => $jasaTerbaru, 'jasaYangSesuai' => $jasaYangSesuai]);
                }
            }
            else
                return redirect('index')->with('status', 'Harap verfikasi akun Anda terlebih dahulu');
        }
        else
        {
            return redirect('login')->with('status', 'Anda Harus Login Untuk Melanjutkan');
        }
    }

    public function history_pembelian(Request $request)
    {
        $history_pembelian = new HistoryPembelian(
                array(
                    'nama_jasa' => $request->get('nama_jasa'),
                    'deskripsi' => $request->get('deskripsi'),
                    'gambar_jasa' => $filename,
                    'harga_jasa' => $hargaBaru,
                    'user_id' => Auth::user()->id,
                    'kategori_jasa_id' => $request->get('user_kategori'),
                ));
        $history_pembelian->save();
    }

    public function checkout()
    {
        function __construct()
        {
            $this->middleware('auth');
        }
        if(Auth::check())
        {
            if(auth()->user()->verified() == true)
            {
                if(!Session::has('cart'))
                {
                    $kategori = Kategori::all();
                    $jasasaya = DB::table('kategori_jasas')
                                                    ->join('jasas', 'kategori_jasas.id', '=', 'jasas.kategori_jasa_id')
                                                    ->select('kategori_jasas.nama_kategori_jasa as nama_kategori',
                                                        'kategori_jasas.id as kategori_id',
                                                        'jasas.id as id_jasa', 
                                                        'jasas.nama_jasa as nama_jasa', 
                                                        'jasas.gambar_jasa as gambar_jasa', 
                                                        'jasas.harga_jasa as harga_jasa',
                                                        'jasas.deskripsi as deskripsi_jasa', 
                                                        'jasas.kategori_jasa_id as kategori_jasa_id')
                                                    ->get();
                    return view('jasaku.checkout', ['jasas' => null, 'jasasaya' => $jasasaya, 'kategori' => $kategori]);
                }
                else
                {
                    $oldCart = Session::get('cart');
                    $cart = new Cart($oldCart);

                    $kategori = Kategori::all();
                    $jasasaya = DB::table('kategori_jasas')
                                                    ->join('jasas', 'kategori_jasas.id', '=', 'jasas.kategori_jasa_id')
                                                    ->select('kategori_jasas.nama_kategori_jasa as nama_kategori',
                                                        'kategori_jasas.id as kategori_id',
                                                        'jasas.id as id_jasa', 
                                                        'jasas.nama_jasa as nama_jasa', 
                                                        'jasas.gambar_jasa as gambar_jasa', 
                                                        'jasas.harga_jasa as harga_jasa',
                                                        'jasas.deskripsi as deskripsi_jasa', 
                                                        'jasas.kategori_jasa_id as kategori_jasa_id')
                                                    ->get();
                    $jasaTerbaru = Jasa::find(DB::table('jasas')->max('id'));
                    $jasaYangSesuai = DB::table('kategori_jasas')
                                                    ->join('jasas', 'kategori_jasas.id', '=', 'jasas.kategori_jasa_id')
                                                    ->select('kategori_jasas.nama_kategori_jasa as nama_kategori',
                                                        'kategori_jasas.id as kategori_id',
                                                        'jasas.id as id_jasa', 
                                                        'jasas.nama_jasa as nama_jasa', 
                                                        'jasas.gambar_jasa as gambar_jasa', 
                                                        'jasas.harga_jasa as harga_jasa',
                                                        'jasas.kategori_jasa_id as kategori_jasa_id')
                                                    ->get();                                  
                    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                    $charactersLength = strlen($characters);
                    $randomString = '';
                    $randomNum = rand(10000,99999);
                    $dateNow = date("Ymd");
                    for ($i = 0; $i < 4; $i++) {
                        $randomString .= $characters[rand(0, $charactersLength - 1)];
                    }
                    return view('jasaku.checkout', ['jasas' => $cart->items, 'totalPrice' => $cart->totalPrice, 'jasasaya' => $jasasaya, 'kategori' => $kategori, 'jasaTerbaru' => $jasaTerbaru, 'jasaYangSesuai' => $jasaYangSesuai, 'randomString' => $randomString, 'randomNum' => $randomNum, 'dateNow' => $dateNow]);
                }
            }
            else
                return redirect('index')->with('status', 'Harap verfikasi akun Anda terlebih dahulu');
        }
        else
        {
            return redirect('login')->with('status', 'Anda Harus Login Untuk Melanjutkan');
        }
    }

    public function kontakkami(Request $request)
    {
        if (Auth::check()) 
        {
            $kategori = Kategori::all();
            $jasasaya = DB::table('kategori_jasas')
                                            ->join('jasas', 'kategori_jasas.id', '=', 'jasas.kategori_jasa_id')
                                            ->select('kategori_jasas.nama_kategori_jasa as nama_kategori',
                                                'kategori_jasas.id as kategori_id',
                                                'jasas.id as id_jasa', 
                                                'jasas.nama_jasa as nama_jasa', 
                                                'jasas.gambar_jasa as gambar_jasa', 
                                                'jasas.harga_jasa as harga_jasa',
                                                'jasas.deskripsi as deskripsi_jasa', 
                                                'jasas.kategori_jasa_id as kategori_jasa_id')
                                            ->get();
            $jasaTerbaru = Jasa::find(DB::table('jasas')->max('id'));
            return view('jasaku.kontakkami', ['jasasaya' => $jasasaya, 'kategori' => $kategori, 'jasaTerbaru' => $jasaTerbaru]);
        }
        else
        {
            $kategori = Kategori::all();
            $jasasaya = DB::table('kategori_jasas')
                                            ->join('jasas', 'kategori_jasas.id', '=', 'jasas.kategori_jasa_id')
                                            ->select('kategori_jasas.nama_kategori_jasa as nama_kategori',
                                                'kategori_jasas.id as kategori_id',
                                                'jasas.id as id_jasa', 
                                                'jasas.nama_jasa as nama_jasa', 
                                                'jasas.gambar_jasa as gambar_jasa', 
                                                'jasas.harga_jasa as harga_jasa',
                                                'jasas.deskripsi as deskripsi_jasa', 
                                                'jasas.kategori_jasa_id as kategori_jasa_id')
                                            ->get();
            $jasaTerbaru = Jasa::find(DB::table('jasas')->max('id'));
            return view('jasaku.kontakkami', ['jasasaya' => $jasasaya, 'kategori' => $kategori, 'jasaTerbaru' => $jasaTerbaru]);
        }
    }

    public function privacy()
    {
        if (Auth::check()) 
        {
            $kategori = Kategori::all();
            $jasasaya = DB::table('kategori_jasas')
                                            ->join('jasas', 'kategori_jasas.id', '=', 'jasas.kategori_jasa_id')
                                            ->select('kategori_jasas.nama_kategori_jasa as nama_kategori',
                                                'kategori_jasas.id as kategori_id',
                                                'jasas.id as id_jasa', 
                                                'jasas.nama_jasa as nama_jasa', 
                                                'jasas.gambar_jasa as gambar_jasa', 
                                                'jasas.harga_jasa as harga_jasa',
                                                'jasas.deskripsi as deskripsi_jasa', 
                                                'jasas.kategori_jasa_id as kategori_jasa_id')
                                            ->get();
            $jasaTerbaru = Jasa::find(DB::table('jasas')->max('id'));
            return view('jasaku.privacy', ['jasasaya' => $jasasaya, 'kategori' => $kategori, 'jasaTerbaru' => $jasaTerbaru]);
        }
        else
        {
            $kategori = Kategori::all();
            $jasasaya = DB::table('kategori_jasas')
                                            ->join('jasas', 'kategori_jasas.id', '=', 'jasas.kategori_jasa_id')
                                            ->select('kategori_jasas.nama_kategori_jasa as nama_kategori',
                                                'kategori_jasas.id as kategori_id',
                                                'jasas.id as id_jasa', 
                                                'jasas.nama_jasa as nama_jasa', 
                                                'jasas.gambar_jasa as gambar_jasa', 
                                                'jasas.harga_jasa as harga_jasa',
                                                'jasas.deskripsi as deskripsi_jasa', 
                                                'jasas.kategori_jasa_id as kategori_jasa_id')
                                            ->get();
            $jasaTerbaru = Jasa::find(DB::table('jasas')->max('id'));
            return view('jasaku.privacy', ['jasasaya' => $jasasaya, 'kategori' => $kategori, 'jasaTerbaru' => $jasaTerbaru]);
        }
    }

    public function history_testi()
    {
        if (Auth::check()) 
        {
            $kategori = Kategori::all();
            $jasasaya = DB::table('kategori_jasas')
                                            ->join('jasas', 'kategori_jasas.id', '=', 'jasas.kategori_jasa_id')
                                            ->select('kategori_jasas.nama_kategori_jasa as nama_kategori',
                                                'kategori_jasas.id as kategori_id',
                                                'jasas.id as id_jasa', 
                                                'jasas.nama_jasa as nama_jasa', 
                                                'jasas.gambar_jasa as gambar_jasa', 
                                                'jasas.harga_jasa as harga_jasa',
                                                'jasas.deskripsi as deskripsi_jasa', 
                                                'jasas.kategori_jasa_id as kategori_jasa_id')
                                            ->get();
            $jasaTerbaru = Jasa::find(DB::table('jasas')->max('id'));
            $testi = Testimonial::all();
            return view('jasaku.history-testimonial', ['jasasaya' => $jasasaya, 'kategori' => $kategori, 'jasaTerbaru' => $jasaTerbaru, 'testi' => $testi]);
        }
        else
        {
            $kategori = Kategori::all();
            $jasasaya = DB::table('kategori_jasas')
                                            ->join('jasas', 'kategori_jasas.id', '=', 'jasas.kategori_jasa_id')
                                            ->select('kategori_jasas.nama_kategori_jasa as nama_kategori',
                                                'kategori_jasas.id as kategori_id',
                                                'jasas.id as id_jasa', 
                                                'jasas.nama_jasa as nama_jasa', 
                                                'jasas.gambar_jasa as gambar_jasa', 
                                                'jasas.harga_jasa as harga_jasa',
                                                'jasas.deskripsi as deskripsi_jasa', 
                                                'jasas.kategori_jasa_id as kategori_jasa_id')
                                            ->get();
            $jasaTerbaru = Jasa::find(DB::table('jasas')->max('id'));
            $testi = Testimonial::all();
            return view('jasaku.history-testimonial', ['jasasaya' => $jasasaya, 'kategori' => $kategori, 'jasaTerbaru' => $jasaTerbaru, 'testi' => $testi]);
        }
    }
}
