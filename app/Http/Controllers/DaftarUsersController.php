<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use App\User;

class DaftarUsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::where('status', 'penjual-pembeli')->get();
        return view('admin.daftar-user', compact('user'));
    }

    public function jsonUser()
    {
        $users = User::paginate();
        // return response()->json(['data'=>$users]);
        if(isset($users)){
            foreach ($users as $kk) {

                echo "<pre> '".json_encode($users, JSON_PRETTY_PRINT)."' </pre>";
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::whereId($id)->firstOrFail();
        $namaUser = $user->name;
        $user->delete();

        return back()->with('status', 'User dengan nama '.$namaUser.' berhasil dihapus!');
    }
}
