<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Pembayaran;
use App\Kategori;
use App\Jasa;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function produk()
    {
        return view('jasaku.produk');
    }

    public function bahasainggris()
    {
        return view('jasaku.bahasa-inggris');
    }

    public function biologi()
    {
        return view('jasaku.biologi');
    }

    public function dokumentasiacara()
    {
        return view('jasaku.dokumentasi-acara');
    }

    public function fotopernikahan()
    {
        return view('jasaku.foto-pernikahan');
    }

    public function fotoulangtahun()
    {
        return view('jasaku.foto-ulang-tahun');
    }

    public function furniture()
    {
        return view('jasaku.furniture');
    }

    public function matematika()
    {
        return view('jasaku.matematika');
    }

    public function mail()
    {
        return view('jasaku.mail');
    }

    public function editprofile()
    {
        return view('jasaku.editprofile');
    }

    public function testcart()
    {
        return view('jasaku.testcart');
    }

    public function qrcode()
    {
        if (Auth::check()) 
        {
            $kategori = Kategori::all();
            $jasasaya = DB::table('kategori_jasas')
                                            ->join('jasas', 'kategori_jasas.id', '=', 'jasas.kategori_jasa_id')
                                            ->select('kategori_jasas.nama_kategori_jasa as nama_kategori',
                                                'kategori_jasas.id as kategori_id',
                                                'jasas.id as id_jasa', 
                                                'jasas.nama_jasa as nama_jasa', 
                                                'jasas.gambar_jasa as gambar_jasa', 
                                                'jasas.harga_jasa as harga_jasa',
                                                'jasas.deskripsi as deskripsi_jasa', 
                                                'jasas.kategori_jasa_id as kategori_jasa_id')
                                            ->get();
            $jasaTerbaru = Jasa::find(DB::table('jasas')->max('id'));
            $dateNow = date("YmdHis");
            $pembayaran = Pembayaran::all();
            return view('riwayat', ['jasasaya' => $jasasaya, 'kategori' => $kategori, 'jasaTerbaru' => $jasaTerbaru, 'pembayaran' => $pembayaran]);
        }
        else
        {
            $kategori = Kategori::all();
            $jasasaya = DB::table('kategori_jasas')
                                            ->join('jasas', 'kategori_jasas.id', '=', 'jasas.kategori_jasa_id')
                                            ->select('kategori_jasas.nama_kategori_jasa as nama_kategori',
                                                'kategori_jasas.id as kategori_id',
                                                'jasas.id as id_jasa', 
                                                'jasas.nama_jasa as nama_jasa', 
                                                'jasas.gambar_jasa as gambar_jasa', 
                                                'jasas.harga_jasa as harga_jasa',
                                                'jasas.deskripsi as deskripsi_jasa', 
                                                'jasas.kategori_jasa_id as kategori_jasa_id')
                                            ->get();
            $jasaTerbaru = Jasa::find(DB::table('jasas')->max('id'));
            $dateNow = date("YmdHis");
            $pembayaran = new Pembayaran();
            return view('riwayat', ['jasasaya' => $jasasaya, 'kategori' => $kategori, 'jasaTerbaru' => $jasaTerbaru, 'pembayaran' => $pembayaran]);
        }
        
    }
}
