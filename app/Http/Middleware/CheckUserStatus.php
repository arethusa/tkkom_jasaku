<?php

namespace App\Http\Middleware;

use Closure;

class CheckUserStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->user()->status=='Admin')
        {
            return redirect('admin');
        }
        else if(auth()->user()->status=='Penjual/Pembeli')
        {
            return redirect('index');
        }
        //return $next($request);
    }
}
