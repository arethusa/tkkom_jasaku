<?php

namespace App;

class Cart 
{
	public $items = null;
	public $totalQty = 0;
	public $totalPrice = 0;
	public $gambarItem;
	public $namaItem;

	public function __construct($oldCart)
	{
		if($oldCart)
		{
			$this->items = $oldCart->items;
			$this->totalQty = $oldCart->totalQty;
			$this->totalPrice = $oldCart->totalPrice;
			$this->gambarItem = $oldCart->gambarItem;
			$this->namaItem = $oldCart->namaItem;
		}
	}

	public function add($item, $id)
	{
		$storedItem = ['qty' => 0, 'harga_jasa' => $item->harga_jasa, 'item' => $item, 'gambar_jasa' => $item->gambar_jasa, 'nama_jasa' => $item->nama_jasa];
		if($this->items)
		{
			if(array_key_exists($id, $this->items))
			{
				$storedItem = $this->items[$id];
			}
		}
		$storedItem['qty']++;
		$storedItem['harga_jasa'] = $item->harga_jasa;
		$this->items[$id] = $storedItem;
		$this->totalQty++;
		$this->totalPrice += $item->harga_jasa;
		$this->gambarItem = $storedItem;
		$this->namaItem = $item->nama_jasa;
	}
}
