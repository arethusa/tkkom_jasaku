<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
/*Route::get('/',  function(){
	return view('index');
});*/
// Route::get('qr-code', function () 
// {
// 	$file = public_path('qr.png');
//   	return QRCode::text('thusa')->setOutfile($file)->png();    
// });
Route::get('history', 'HomeController@qrcode');
Route::get('/', 'PagesController@index');
Route::get('/index', 'PagesController@index')->name('index');
Route::resource('edit-profil', 'UsersController');
Route::resource('kategori-jasa', 'KategorisController');
Route::resource('tambah-jasa', 'TambahJasaController');
Route::resource('pembayaran-jasa', 'PembayaranController');
Route::resource('kontak-kami', 'KontakKamiController');
Route::resource('isi-testimonial', 'TestimonialController');
Route::get('/detail-jasa/{id}', 'PagesController@detailJasa')->name('detail-jasa');
Route::get('/keranjang-anda', 'PagesController@cart');
Route::get('/produk', 'PagesController@produk');
Route::get('search', 'PagesController@search');
Route::get('/hasilPencarian', 'PagesController@hasilPencarian');
Route::get('/checkout', 'PagesController@checkout');
Route::get('/pembayaran', 'PagesController@pembayaran');
Route::get('/bahasa-inggris', 'HomeController@bahasainggris');
Route::get('/biologi', 'HomeController@biologi');
Route::get('/dokumentasi-acara', 'HomeController@dokumentasiacara');
Route::get('/foto-pernikahan', 'HomeController@fotopernikahan');
Route::get('/foto-ulang-tahun', 'HomeController@fotoulangtahun');
Route::get('/furniture', 'HomeController@furniture');
Route::get('/matematika', 'HomeController@matematika');
Route::get('/mail', 'HomeController@mail');
Route::get('/editprofile', 'HomeController@editprofile');
Route::get('/cart/{id}', 'PagesController@AddToCart');
Route::get('/remove/{id}', 'PagesController@remove');
Route::get('/kontakkami', 'PagesController@kontakkami');
Route::get('/privacy-policy', 'PagesController@privacy');
Route::get('/verify/{token}', 'VerifyController@verify')->name('verify');

/** ADMIN */
Route::prefix('admin')->group(function() {
	Route::resource('dashboard', 'AdminsController');
	Route::resource('daftar-jasa', 'DaftarJasasController');
	Route::resource('daftar-user', 'DaftarUsersController');
	Route::resource('daftar-order', 'DaftarOrdersController');
	Route::resource('daftar-bukti-pembayaran', 'DaftarPembayaransController');
});